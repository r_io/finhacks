console.log("FOREGROUND");

class Modal{
    constructor(title){
        this.title=title;
        this.fields=[];
    }

    addInput(type,label){
        this.$elementFormGroup = jQuery('<div/>',{
            class: "form-group"
        });

            this.$elementLabel = jQuery('<label/>').appendTo(this.$elementFormGroup);
            this.$elementLabel.html(label);

            this.$elementInput = jQuery('<input/>',{
                type: type,
                class:"form-control"
            }).appendTo(this.$elementFormGroup);

        this.fields.push(this.$elementFormGroup);

        return this;
    }

    addSelect(label,options){
        this.$elementFormGroup = jQuery('<div/>',{
            class: "form-group"
        });

            this.$elementLabel = jQuery('<label/>').appendTo(this.$elementFormGroup);
            this.$elementLabel.html(label);

            this.$elementSelect = jQuery('<select/>',{
                class:"form-control"
            }).appendTo(this.$elementFormGroup);

                for (var index = 0; index < options.length; index++) {
                    this.$elementOption =  jQuery('<option/>').appendTo(this.$elementSelect);
                    this.$elementOption.html(options[index]);
                }

        this.fields.push(this.$elementFormGroup);

        return this;
    }

    createOn(element){
        this.$elementModal = jQuery('<div/>',{
            id: "SMPAY",
            class: "modal fade",
            role: "dialog"
        }).appendTo(element);

            this.$elementModalDialog = jQuery('<div/>',{
                class: "modal-dialog"
            }).appendTo(this.$elementModal);

                this.$elementModalContent = jQuery('<div/>',{
                    class: "modal-content",
                }).appendTo(this.$elementModalDialog);

                    this.$elementModalHeader = jQuery('<div/>',{
                        class: "modal-header",
                    }).appendTo(this.$elementModalContent);

                        this.$elementModalHeaderButtonClose = jQuery('<button/>',{
                            class: "close",
                            "data-dismiss": "modal"
                        }).appendTo(this.$elementModalHeader);
                        this.$elementModalHeaderButtonClose.html("&times;");

                        this.$elementModalHeaderTitle = jQuery('<h4/>',{
                            class: "modal-title"
                        }).appendTo(this.$elementModalHeader);
                        this.$elementModalHeaderTitle.html(this.title);

                    this.$elementModalBody = jQuery('<div/>',{
                        class: "modal-body",
                    }).appendTo(this.$elementModalContent);

                        for (var index = 0; index < this.fields.length; index++) {
                            this.fields[index].appendTo(this.$elementModalBody);
                        }

                    this.$elementModalFooter = jQuery('<div/>',{
                        class: "modal-footer",
                    }).appendTo(this.$elementModalContent);

                        this.$elementModalHeaderButtonSubmit = jQuery('<button/>',{
                            type: "button",
                            class: "btn btn-primary",
                            "data-dismiss": "modal"
                        }).appendTo(this.$elementModalFooter);
                        this.$elementModalHeaderButtonSubmit.html("Submit");

        return this.$elementModal;
    }
}

$(function(){
    new Modal("HALO")
        .addInput("text","Name")
        .addInput("password","Password")
        .addSelect("Gender",["Male","Female"])
        .createOn("body")
        .modal("show");
})