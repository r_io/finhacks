<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('api')->middleware('cors')->group(function(){
    Route::post('/user/login','UserController@login');
    Route::post('/user/insert', 'UserController@insert');
    Route::post('/user/update/{id}', 'UserController@update');
    Route::get('/user/balance/{id}', 'AllController@getBalance');
    Route::get('/user/profile/{facebook_id}', 'UserController@profile');

    Route::post('/product/insert', 'ProductController@insert');
    Route::post('/product/update/{id}', 'ProductController@update');
    Route::get('/product/report/{id}','ProductController@report');
    
    Route::post('/transaction/insert', 'TransactionController@insert');
    Route::post('/transaction/update/{id}', 'TransactionController@update');
    Route::post('/transaction/transfer', 'AllController@transfer');

    Route::post('/sell','AllController@sell');
    Route::post('/buy','AllController@buy');
    Route::get('/verify/{product_id}','AllController@verify');
});
