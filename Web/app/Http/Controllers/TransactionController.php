<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\BcaApiController;
use Illuminate\Http\Request;

use App\Transaction;

class TransactionController extends Controller
{
    public function insert(Request $request){
        $credential = (new BcaApiController())->data;

        $parameters = $request->all();

        $object = new Transaction();
        foreach($parameters as $key=>$value){
            $object->$key = $value;
        }
        $object->save();

        return $object;
    }

    public function update($id,Request $request){
        $parameters = $request->all();

        $object = Transaction::find($id);
        foreach($parameters as $key=>$value){
            $object->$key = $value;
        }
        $object->save();

        return $object;
    }
}
