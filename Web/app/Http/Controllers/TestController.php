<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index(){

        

        $clientId = '00a2cecf-57a9-495d-b337-05379481cea2';
        $clientSecret = '90f866f0-0bb1-419f-bfcc-abd3ce65d0e1';
        $apiKey = '1b6e44be-df70-4013-8a75-3d7abd2a8046';
        $apiSecret = '60766ed9-2480-4f47-ab3f-68a5a719b54d';
        $corporateId = 'FHK1ID';
        $accessCode = 'rqdJtRKxhEeOk9jsxJCx';
        $branchCode = 'FHK1ID01';
        $userId = 'FHK1OPR1';
        $localId = '0405';
        $accNumber = '8220002200';
        $accName = 'FINHACKS01';


        $builder = new \Bca\Api\Sdk\Fire\FireApiConfigBuilder();
        $builder->baseApiUri('https://api.finhacks.id/');
        $builder->baseOAuth2Uri('https://api.finhacks.id/');
        $builder->clientId($clientId);
        $builder->clientSecret($clientSecret);
        $builder->apiKey($apiKey);
        $builder->apiSecret($apiSecret);
        $builder->origin('yourdomain.com');
        $builder->corporateID($corporateId);
        $builder->accessCode($accessCode);
        $builder->branchCode($branchCode);
        $builder->userID($userId);
        $builder->localID($localId);

        $config = $builder->build();

        $fireApi = new \Bca\Api\Sdk\Fire\FireApi($config);

        $senderDetails = new \Bca\Api\Sdk\Fire\Models\Requests\TransferSenderDetailsPayload();
        $senderDetails->setFirstName('John');
        $senderDetails->setLastName('Doe');
        $senderDetails->setDateOfBirth('2000-05-20');
        $senderDetails->setAddress1('HILLS STREET 1');
        $senderDetails->setAddress2('');
        $senderDetails->setCity('HOLLYWOOD');
        $senderDetails->setStateID('');
        $senderDetails->setPostalCode('');
        $senderDetails->setCountryID('US');
        $senderDetails->setMobile('');
        $senderDetails->setIdentificationType('');
        $senderDetails->setIdentificationNumber('');
        $senderDetails->setAccountNumber($accNumber);

        $beneficiaryDetails = new \Bca\Api\Sdk\Fire\Models\Requests\TransferBeneficiaryDetailsPayload();
        $beneficiaryDetails->setName('Sam');
        $beneficiaryDetails->setDateOfBirth('2000-05-20');
        $beneficiaryDetails->setAddress1('HILLS STREET 1');
        $beneficiaryDetails->setAddress2('');
        $beneficiaryDetails->setCity('HOLLYWOOD');
        $beneficiaryDetails->setStateID('');
        $beneficiaryDetails->setPostalCode('');
        $beneficiaryDetails->setCountryID('ID');
        $beneficiaryDetails->setMobile('');
        $beneficiaryDetails->setIdentificationType('');
        $beneficiaryDetails->setIdentificationNumber('');
        $beneficiaryDetails->setNationalityID('');
        $beneficiaryDetails->setOccupation('');
        $beneficiaryDetails->setBankCodeType('BIC');
        $beneficiaryDetails->setBankCodeValue('CENAIDJAXXX');
        $beneficiaryDetails->setBankCountryID('ID');
        $beneficiaryDetails->setBankAddress('');
        $beneficiaryDetails->setBankCity('');
        $beneficiaryDetails->setAccountNumber('010203040506');

        $transactionDetails = new \Bca\Api\Sdk\Fire\Models\Requests\TransferTransactionDetailsPayload();
        $transactionDetails->setCurrencyID('IDR');
        $transactionDetails->setAmount('100000.00');
        $transactionDetails->setPurposeCode('011');
        $transactionDetails->setDescription1('');
        $transactionDetails->setDescription2('');
        $transactionDetails->setDetailOfCharges('SHA');
        $transactionDetails->setSourceOfFund('');
        $transactionDetails->setFormNumber('RT254 ID1');

        $payload = new \Bca\Api\Sdk\Fire\Models\Requests\TransferToAccountPayload();
        $payload->setSenderDetails($senderDetails);
        $payload->setBeneficiaryDetails($beneficiaryDetails);
        $payload->setTransactionDetails($transactionDetails);

        $response = $fireApi->transferToAccount($payload);
    }
}
