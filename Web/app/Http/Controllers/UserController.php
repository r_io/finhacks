<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class UserController extends Controller
{
    public function profile($facebook_id){
        return json_encode(User::where("facebook_id","=",$facebook_id)->first());
    }

    public function login(Request $request){
        $user=new User();
        $user->facebook_tooken = $request->input("facebook_token");

        //generate_bca_token
        $user->bca_token = "testing";
        $user->save();

        return $user;
    }

    public function insert(Request $request){
        $parameters = $request->all();

        $object = new User();
        foreach($parameters as $key=>$value){
            $object->$key = $value;
        }
        $object->save();

        return $object;
    }

    public function update($id,Request $request){
        $parameters = $request->all();

        $object = User::find($id);
        foreach($parameters as $key=>$value){
            $object->$key = $value;
        }
        $object->save();

        return $object;
    }
}
