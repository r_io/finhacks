<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;
use App\Transaction;

class ProductController extends Controller
{
    public function report($id){
        $product=Product::find($id);
        $data=Transaction::where("productId","=",$id)->join('users','users.id','=','transactions.userId')
            ->select(['transactions.id', 'transactions.quantity', 'users.first_name', 'users.last_name', 'users.address', 'users.mobile'])
            ->get();
        return view("report")->with(compact(['data','product']));
    }

    public function insert(Request $request){
        $parameters = $request->all();

        $object = new Product();
        foreach($parameters as $key=>$value){
            $object->$key = $value;
        }
        $object->save();

        return $object;
    }

    public function update($id,Request $request){
        $parameters = $request->all();

        $object = Product::find($id);
        foreach($parameters as $key=>$value){
            $object->$key = $value;
        }
        $object->save();

        return $object;
    }
}
