<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Product;
use App\Transaction;

require '../vendor/autoload.php';

class AllController extends Controller
{

    public function verify($product_id){
        $product=Product::find($product_id);
        if($product==null)return null;
        return(User::find($product->userId)->facebook_id);
    }

    public function sell(Request $request){
        
        //User
        $user = User::where("facebook_id","=",$request->input("facebook_id"))->first();
        if($user==null){
            $user=new User();
            $user->facebook_id = $request->input("facebook_id");
        }
        $user->account_number = $request->input("account_number");
        $user->first_name = $request->input("first_name");
        $user->last_name = $request->input("last_name");
        $user->address = $request->input("address");
        $user->mobile = $request->input("mobile");
        $user->save();

        //Product
        $product = new Product();
        $product->name = $request->input("name");
        $product->desc = $request->input("desc");
        $product->price = $request->input("price");
        $product->userId = $user->id;
        $product->save();

        return $product;
    }

    public function buy(Request $request){
        //Buyer
        $user = User::where("facebook_id","=",$request->input("facebook_id"))->first();
        if($user==null){
            $user=new User();
            $user->facebook_id = $request->input("facebook_id");
        }
        $user->account_number = $request->input("account_number");
        $user->first_name = $request->input("first_name");
        $user->last_name = $request->input("last_name");
        $user->address = $request->input("address");
        $user->mobile = $request->input("mobile");
        $user->save();

        //Transaction
        $transaction=new Transaction();
        $transaction->quantity = $request->input("quantity");
        $transaction->productId = $request->input("productid");
        $transaction->userId = $user->id;

        //Product
        $product=Product::find($transaction->productId);

        //Seller
        $seller=User::find($product->userId);

        //Get Balance
        $client = new \GuzzleHttp\Client();
        $res = $client->get('http://182.16.165.100/index.php/api/user/balance/'.$user->account_number);
        $balance = intval($res->getBody()->getContents());

        if($balance < $transaction->quantity * $product->price){
            return "Balance Not Enough";
        }

        //Transfer
        $client = new \GuzzleHttp\Client();
        $res = $client->post('http://182.16.165.100/index.php/api/transaction/transfer/', [
            'json' => [
                'sender_first_name' => $user->first_name,
                'sender_last_name' => $user->last_name,
                'sender_address' => $user->address,
                'sender_city' => 'N/A',
                'sender_mobile' => $user->mobile,
                'sender_account_number' => $user->account_number,
                'receiver_first_name' => $seller->first_name,
                'receiver_last_name' => $seller->last_name,
                'receiver_address' => $seller->address,
                'receiver_mobile' => $seller->mobile,
                'receiver_account_number' => $seller->account_number,
                'amount' => $transaction->quantity * $product->price,
                'product_name' => $product->name
            ]
        ]);
        // TODO: if fails, and add reference number on migration
        $res = json_decode($res->getBody()->getContents());
        if($res[0]!='Success')
            return $res[0];

        $transaction->refNumber = $res[1];
        $transaction->save();

        return $res[0];
    }

    public function getBalance($id){
        $fireApi = $this->generateHeader();

        $FIDetails = new \Bca\Api\Sdk\Fire\Models\Requests\BalanceFIDetailsPayload();
        $FIDetails->setAccountNumber($id);

        $payload = new \Bca\Api\Sdk\Fire\Models\Requests\BalanceInquiryPayload();
        $payload->setFIDetails($FIDetails);

        $response = $fireApi->balanceInquiry($payload);
        return $response->getFIDetails()->getAccountBalance();
    }

    public function transfer(Request $request)
    {
        /* transfer */
        $senderDetails = new \Bca\Api\Sdk\Fire\Models\Requests\TransferSenderDetailsPayload();
        $senderDetails->setFirstName($request->input("sender_first_name"));
        $senderDetails->setLastName($request->input("sender_last_name"));
        $senderDetails->setDateOfBirth('');
        $senderDetails->setAddress1($request->input("sender_address"));
        $senderDetails->setAddress2('');
        $senderDetails->setCity($request->input("sender_city"));
        $senderDetails->setStateID('');
        $senderDetails->setPostalCode($request->input("sender_postal_code"));
        $senderDetails->setCountryID('US');
        $senderDetails->setMobile($request->input("sender_mobile"));
        $senderDetails->setIdentificationType('');
        $senderDetails->setIdentificationNumber('');
        $senderDetails->setAccountNumber($request->input("sender_account_number"));

        $beneficiaryDetails = new \Bca\Api\Sdk\Fire\Models\Requests\TransferBeneficiaryDetailsPayload();
        $beneficiaryDetails->setName($request->input("receiver_first_name") . ' ' . $request->input("receiver_last_name"));
        $beneficiaryDetails->setDateOfBirth('');
        $beneficiaryDetails->setAddress1($request->input("receiver_address"));
        $beneficiaryDetails->setAddress2('');
        $beneficiaryDetails->setCity($request->input("receiver_city"));
        $beneficiaryDetails->setStateID('');
        $beneficiaryDetails->setPostalCode($request->input("receiver_postal_code"));
        $beneficiaryDetails->setCountryID('ID');
        $beneficiaryDetails->setMobile($request->input("receiver_mobile"));
        $beneficiaryDetails->setIdentificationType('');
        $beneficiaryDetails->setIdentificationNumber('');
        $beneficiaryDetails->setNationalityID('');
        $beneficiaryDetails->setOccupation('');
        $beneficiaryDetails->setBankCodeType('BIC');
        $beneficiaryDetails->setBankCodeValue('CENAIDJAXXX');
        $beneficiaryDetails->setBankCountryID('ID');
        $beneficiaryDetails->setBankAddress('');
        $beneficiaryDetails->setBankCity('');
        $beneficiaryDetails->setAccountNumber($request->input("receiver_account_number"));

        $description = 'Buying '.$request->input('product_name');
        $transactionDetails = new \Bca\Api\Sdk\Fire\Models\Requests\TransferTransactionDetailsPayload();
        $transactionDetails->setCurrencyID('IDR');
        $transactionDetails->setAmount($request->input("amount"));
        $transactionDetails->setPurposeCode('011');
        $transactionDetails->setDescription1(substr($description, 0, 35));
        $transactionDetails->setDescription2(strlen($description)>35?substr($description, 36, 70):'');
        $transactionDetails->setDetailOfCharges('SHA');
        $transactionDetails->setSourceOfFund('');
        $transactionDetails->setFormNumber(time() . '');

        $payload = new \Bca\Api\Sdk\Fire\Models\Requests\TransferToAccountPayload();
        $payload->setSenderDetails($senderDetails);
        $payload->setBeneficiaryDetails($beneficiaryDetails);
        $payload->setTransactionDetails($transactionDetails);

        $fireApi = $this->generateHeader();
        $response = $fireApi->transferToAccount($payload);

        $refNumber = $response->getStatusMessage()=='Success'?$response->getTransactionDetails()->getReferenceNumber() : 0;

        return [$response->getStatusMessage(), $refNumber];
    }
    public function getTransaction(Request $request){
        $transaction = $request->input('transaactionId');

        $transactions = Transaction::find($transaction);//Transaction::where('created_at', '>=', date('Y-m-d', strtotime('-1 month')))
            //->get();

        $fireApi = $this->generateHeader();
        $results = [];
        foreach ($transactions as $transaction){
            $transactionDetails = new \Bca\Api\Sdk\Fire\Models\Requests\TransactionDetailsPayload();
            $transactionDetails->setInquiryBy('B');
            $transactionDetails->setInquiryValue($transaction->refNumber);

            $payload = new \Bca\Api\Sdk\Fire\Models\Requests\TransactionInquiryPayload();
            $payload->setTransactionDetails($transactionDetails);

            $response = $fireApi->transactionInquiry($payload);
            $results[$transaction->id] = [
                'transaction' => $transaction,
                'detail' => $response
            ];
        }
        return $results;
    }

    public function generateHeader(){
        $credential = (new Auth\BcaApiController())->data;
        $builder = new \Bca\Api\Sdk\Fire\FireApiConfigBuilder();
        $builder->baseApiUri('https://api.finhacks.id/');
        $builder->baseOAuth2Uri('https://api.finhacks.id/');
        $builder->clientId($credential['clientId']);
        $builder->clientSecret($credential['clientSecret']);
        $builder->apiKey($credential['apiKey']);
        $builder->apiSecret($credential['apiSecret']);
        $builder->origin('yourdomain.com');
        $builder->corporateID($credential['corporateId']);
        $builder->accessCode($credential['accessCode']);
        $builder->branchCode($credential['branchCode']);
        $builder->userID($credential['userId']);
        $builder->localID($credential['localId']);

        $config = $builder->build();
        return new \Bca\Api\Sdk\Fire\FireApi($config);
    }
}
