<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>SMPAY</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #000;
                color: #fff;
                font-family: 'Raleway', sans-serif;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            li,label {
                color: #fff;
                padding: 0 25px;
                font-size: 18px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <h2>Product Detail</h2>
                <table style="width: 100%; margin-bottom: 30px;">
                    <tr>
                        <td style="font-weight: bold;">Name</td>
                        <td>{{ $product->name }}</td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;"><div style="max-width: 500px;">Description</div></td>
                        <td>{{ $product->desc }}</td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">Price</td>
                        <td>{{ $product->price }}</td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            QUANTITY
                        </th>
                        <th>
                            NAME
                        </th>
                        <th>
                            ADDRESS
                        </th>
                        <th>
                            PHONE
                        </th>
                    </tr>
                    @foreach($data as $row)
                    <tr>
                        <td>
                            {{ $row->id }}
                        </td>
                        <td>
                            {{ $row->quantity }}
                        </td>
                        <td>
                            {{ $row->first_name }} {{ $row->last_name }}
                        </td>
                        <td>
                            {{ $row->address }}
                        </td>
                        <td>
                            {{ $row->mobile }}
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </body>
</html>
