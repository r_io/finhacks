<?php
namespace Bca\Api\Sdk\BusinessBanking;

use Bca\Api\Sdk\Common\Config\BaseApiConfigBuilder;
use Bca\Api\Sdk\Common\Utils\Validator;

class BusinessBankingApiConfigBuilder extends BaseApiConfigBuilder
{
    protected $corporateID;

    /**
     * @return string
     */
    public function getCorporateID()
    {
        return $this->corporateID;
    }

    /**
     * @param string $corporateID
     * @return BusinessBankingApiConfigBuilder
     */
    public function corporateID($corporateID)
    {
        $this->corporateID = (string)$corporateID;
        return $this;
    }

    /**
     * @return BusinessBankingApiConfig
     * @throws InvalidArgumentException
     */
    public function build()
    {
        $this->validateProperties();
        return new BusinessBankingApiConfig($this);
    }

    protected function validateProperties()
    {
        parent::validateProperties();
        Validator::shouldNotNullOrEmpty($this->corporateID);
    }
}