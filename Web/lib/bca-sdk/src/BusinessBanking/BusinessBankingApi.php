<?php
namespace Bca\Api\Sdk\BusinessBanking;

use Bca\Api\Sdk\BusinessBanking\BusinessBankingApiConfig;
use Bca\Api\Sdk\BusinessBanking\Models\Requests\AccountValidationParams;
use Bca\Api\Sdk\BusinessBanking\Models\Requests\StatementParams;
use Bca\Api\Sdk\BusinessBanking\Models\Requests\TransferPayload;
use Bca\Api\Sdk\BusinessBanking\Models\Responses\BalanceResponse;
use Bca\Api\Sdk\BusinessBanking\Models\Responses\EmptyResponse;
use Bca\Api\Sdk\BusinessBanking\Models\Responses\StatementResponse;
use Bca\Api\Sdk\BusinessBanking\Models\Responses\TransferResponse;
use Bca\Api\Sdk\Common\Exceptions\ApiRequestException;
use Bca\Api\Sdk\Common\Http\DefaultHttpClient;
use Bca\Api\Sdk\Common\Http\HttpClient;
use Bca\Api\Sdk\Common\Http\RequestHelper;
use Bca\Api\Sdk\Common\OAuth2\Grant\Client\ClientCredentialsOAuth2Client;
use Bca\Api\Sdk\Common\OAuth2\Grant\Client\ClientCredentialsOAuth2Details;
use Bca\Api\Sdk\Common\Utils\DefaultJsonParser;
use Bca\Api\Sdk\Common\Utils\Logger\LoggerFactory;

class BusinessBankingApi
{
    use \Bca\Api\Sdk\Common\Utils\HasGetClass;
    use \Bca\Api\Sdk\Common\Utils\HasLogger;

    const ACCOUNT_VALID_HTTP_CODE = 200;

    protected $config;
    protected $helper;

    public function __construct(BusinessBankingApiConfig $config)
    {
        static::getLogger()->info('Configure BusinessBanking Api');
        static::getLogger()->debug('BusinessBankingApi configs: ' . $config);

        $this->config = $config;

        $httpClient = new DefaultHttpClient();
        $details = new ClientCredentialsOAuth2Details($config);
        $oAuth2Client = new ClientCredentialsOAuth2Client($details, $httpClient);

        $this->helper = new RequestHelper($config, $oAuth2Client, $httpClient, new DefaultJsonParser());
    }

    public function setHttpClient(HttpClient $httpClient)
    {
        $details = new ClientCredentialsOAuth2Details($this->config);
        $oAuth2Client = new ClientCredentialsOAuth2Client($details, $httpClient);

        $this->helper = new RequestHelper($this->config, $oAuth2Client, $httpClient, new DefaultJsonParser());
    }

    public function getBalance(array $accountNumbers)
    {
        static::getLogger()->info('Handle getBalance');

        $corporateID = $this->config->getCorporateID();
        $accounts = implode(',', $accountNumbers);
        $url = sprintf($this->config->getBalanceUri(), $corporateID, $accounts);
        return $this->helper->fetchAndWrapResponse(
            'GET',
            $url,
            '',
            BalanceResponse::getClass()
        );
    }

    public function getStatement($accountNumber, StatementParams $params)
    {
        static::getLogger()->info('Handle getStatement');

        $corporateID = $this->config->getCorporateID();
        $url = sprintf($this->config->getStatementUri(), $corporateID, $accountNumber) .
            '?' . (string)$params;
        return $this->helper->fetchAndWrapResponse(
            'GET',
            $url,
            '',
            StatementResponse::getClass()
        );
    }

    public function transferFund(TransferPayload $transfer)
    {
        static::getLogger()->info('Handle transferFund');

        $transfer->setCorporateID($this->config->getCorporateID());
        $url = $this->config->getTransferUri();
        return $this->helper->fetchAndWrapResponse(
            'POST',
            $url,
            $transfer,
            TransferResponse::getClass()
        );
    }

    /**
     * @return true if account exists
     * @throws ApiRequestException if account not found
     */
    public function isAccountValid($accountNumber, AccountValidationParams $params)
    {
        static::getLogger()->info('Handle inquiryTransfer');

        $corporateID = $this->config->getCorporateID();
        $url = sprintf($this->config->getAccountValidationUri(), $corporateID, $accountNumber) .
            '?' . (string)$params;

        return (bool)$this->helper->fetchAndWrapResponse(
            'GET',
            $url,
            '',
            EmptyResponse::getClass()
        );
    }
}