<?php
namespace Bca\Api\Sdk\BusinessBanking\Models\Requests;

use Bca\Api\Sdk\Common\Utils\Formatter;

class StatementParams
{
    const START_DATE = 'StartDate';
    const END_DATE = 'EndDate';

    protected $startDate;
    protected $endDate;

    public function __toString()
    {
        return Formatter::httpQuery([
            static::START_DATE => $this->startDate,
            static::END_DATE => $this->endDate,
        ]);
    }

    /**
     * @return string
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param string $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = (string)$startDate;
    }

    /**
     * @return string
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param string $endDate
     */
    public function setEndDate($endDate)
    {
        $this->endDate = (string)$endDate;
    }
}