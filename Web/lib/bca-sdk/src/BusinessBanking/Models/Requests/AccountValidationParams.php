<?php
namespace Bca\Api\Sdk\BusinessBanking\Models\Requests;

use Bca\Api\Sdk\Common\Utils\Formatter;

class AccountValidationParams
{
    const ACTION = 'Action';
    const BY = 'By';
    const VALUE = 'Value';

    protected $action;
    protected $by;
    protected $value;

    public function __toString()
    {
        return Formatter::httpQuery([
            static::ACTION => $this->action,
            static::BY => $this->by,
            static::VALUE => $this->value,
        ]);
    }

    public function getAction()
    {
        return $this->action;
    }

    public function setAction($action)
    {
        $this->action = (string)$action;
    }

    public function getBy()
    {
        return $this->by;
    }

    public function setBy($by)
    {
        $this->by = (string)$by;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
        $this->value = (string)$value;
    }
}