<?php
namespace Bca\Api\Sdk\BusinessBanking\Models\Requests;

use Bca\Api\Sdk\Common\Utils\JsonSerializablePayload;

class TransferPayload extends JsonSerializablePayload
{
    protected $corporateID;
    protected $sourceAccountNumber;
    protected $transactionID;
    protected $transactionDate;
    protected $referenceID;
    protected $currencyCode;
    protected $amount;
    protected $beneficiaryAccountNumber;
    protected $remark1;
    protected $remark2;

    /**
     * @return string
     */
    public function getCorporateID()
    {
        return $this->corporateID;
    }

    /**
     * @param string $corporateID
     */
    public function setCorporateID($corporateID)
    {
        $this->corporateID = (string)$corporateID;
    }

    /**
     * @return string
     */
    public function getSourceAccountNumber()
    {
        return $this->sourceAccountNumber;
    }

    /**
     * @param string $sourceAccountNumber
     */
    public function setSourceAccountNumber($sourceAccountNumber)
    {
        $this->sourceAccountNumber = (string)$sourceAccountNumber;
    }

    /**
     * @return string
     */
    public function getTransactionID()
    {
        return $this->transactionID;
    }

    /**
     * @param string $transactionID
     */
    public function setTransactionID($transactionID)
    {
        $this->transactionID = (string)$transactionID;
    }

    /**
     * @return string
     */
    public function getTransactionDate()
    {
        return $this->transactionDate;
    }

    /**
     * @param string $transactionDate
     */
    public function setTransactionDate($transactionDate)
    {
        $this->transactionDate = (string)$transactionDate;
    }

    /**
     * @return string
     */
    public function getReferenceID()
    {
        return $this->referenceID;
    }

    /**
     * @param string $referenceID
     */
    public function setReferenceID($referenceID)
    {
        $this->referenceID = (string)$referenceID;
    }

    /**
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * @param string $currencyCode
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = (string)$currencyCode;
    }

    /**
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param string $amount
     */
    public function setAmount($amount)
    {
        $this->amount = (string)$amount;
    }

    /**
     * @return string
     */
    public function getBeneficiaryAccountNumber()
    {
        return $this->beneficiaryAccountNumber;
    }

    /**
     * @param string $beneficiaryAccountNumber
     */
    public function setBeneficiaryAccountNumber($beneficiaryAccountNumber)
    {
        $this->beneficiaryAccountNumber = (string)$beneficiaryAccountNumber;
    }

    /**
     * @return string
     */
    public function getRemark1()
    {
        return $this->remark1;
    }

    /**
     * @param string $remark1
     */
    public function setRemark1($remark1)
    {
        $this->remark1 = (string)$remark1;
    }

    /**
     * @return string
     */
    public function getRemark2()
    {
        return $this->remark2;
    }

    /**
     * @param string $remark2
     */
    public function setRemark2($remark2)
    {
        $this->remark2 = (string)$remark2;
    }
}