<?php
namespace Bca\Api\Sdk\BusinessBanking\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class BalanceResponse extends JsonUnserializableResponse
{
    public static function fromJson($json)
    {
        $result = parent::fromJson($json);
        $details = [];
        if ($result->accountDetailDataSuccess) foreach ($result->accountDetailDataSuccess as $jsonDetail) {
            $details[] = AccountDetailDataSuccess::fromJson(json_encode($jsonDetail));
        }
        $result->accountDetailDataSuccess = $details;
        $details = [];
        if ($result->accountDetailDataFailed) foreach ($result->accountDetailDataFailed as $jsonDetail) {
            $details[] = AccountDetailDataFailed::fromJson(json_encode($jsonDetail));
        }
        $result->accountDetailDataFailed = $details;
        return $result;
    }

    protected $accountDetailDataSuccess;
    protected $accountDetailDataFailed;

    /**
     * @return array
     */
    public function getAccountDetailDataSuccess()
    {
        return $this->accountDetailDataSuccess;
    }

    /**
     * @return array
     */
    public function getAccountDetailDataFailed()
    {
        return $this->accountDetailDataFailed;
    }
}