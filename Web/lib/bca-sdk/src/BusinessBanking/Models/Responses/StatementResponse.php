<?php
namespace Bca\Api\Sdk\BusinessBanking\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class StatementResponse extends JsonUnserializableResponse
{
    public static function fromJson($json)
    {
        $result = parent::fromJson($json);
        $details = [];
        foreach ($result->data as $jsonDetail) {
            $details[] = StatementData::fromJson(json_encode($jsonDetail));
        }
        $result->data = $details;
        return $result;
    }

    protected $startDate;
    protected $endDate;
    protected $currency;
    protected $startBalance;
    protected $data;

    /**
     * @return string
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @return string
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return string
     */
    public function getStartBalance()
    {
        return $this->startBalance;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }
}