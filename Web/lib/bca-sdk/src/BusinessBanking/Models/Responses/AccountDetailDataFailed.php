<?php
namespace Bca\Api\Sdk\BusinessBanking\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class AccountDetailDataFailed extends JsonUnserializableResponse
{
    protected $english;
    protected $indonesian;
    protected $accountNumber;

    /**
     * @return string
     */
    public function getEnglish()
    {
        return $this->english;
    }

    /**
     * @return string
     */
    public function getIndonesian()
    {
        return $this->indonesian;
    }

    /**
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }
}