<?php
namespace Bca\Api\Sdk\BusinessBanking\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class StatementData extends JsonUnserializableResponse
{
    protected $transactionDate;
    protected $branchCode;
    protected $transactionType;
    protected $transactionAmount;
    protected $transactionName;
    protected $trailer;

    /**
     * @return string
     */
    public function getTransactionDate()
    {
        return $this->transactionDate;
    }

    /**
     * @return string
     */
    public function getBranchCode()
    {
        return $this->branchCode;
    }

    /**
     * @return string
     */
    public function getTransactionType()
    {
        return $this->transactionType;
    }

    /**
     * @return string
     */
    public function getTransactionAmount()
    {
        return $this->transactionAmount;
    }

    /**
     * @return string
     */
    public function getTransactionName()
    {
        return $this->transactionName;
    }

    /**
     * @return string
     */
    public function getTrailer()
    {
        return $this->trailer;
    }
}