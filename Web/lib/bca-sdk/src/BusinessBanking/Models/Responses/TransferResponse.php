<?php
namespace Bca\Api\Sdk\BusinessBanking\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class TransferResponse extends JsonUnserializableResponse
{
    protected $transactionID;
    protected $transactionDate;
    protected $referenceID;
    protected $status;

    /**
     * @return string
     */
    public function getTransactionID()
    {
        return $this->transactionID;
    }

    /**
     * @return string
     */
    public function getTransactionDate()
    {
        return $this->transactionDate;
    }

    /**
     * @return string
     */
    public function getReferenceID()
    {
        return $this->referenceID;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }
}