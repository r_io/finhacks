<?php
namespace Bca\Api\Sdk\BusinessBanking\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class EmptyResponse extends JsonUnserializableResponse
{
}