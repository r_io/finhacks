<?php
namespace Bca\Api\Sdk\BusinessBanking\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class AccountDetailDataSuccess extends JsonUnserializableResponse
{
    protected $accountNumber;
    protected $currency;
    protected $balance;
    protected $availableBalance;
    protected $floatAmount;
    protected $holdAmount;
    protected $plafon;

    /**
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return string
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @return string
     */
    public function getAvailableBalance()
    {
        return $this->availableBalance;
    }

    /**
     * @return string
     */
    public function getFloatAmount()
    {
        return $this->floatAmount;
    }

    /**
     * @return string
     */
    public function getHoldAmount()
    {
        return $this->holdAmount;
    }

    /**
     * @return string
     */
    public function getPlafon()
    {
        return $this->plafon;
    }
}