<?php
namespace Bca\Api\Sdk\BusinessBanking;

use Bca\Api\Sdk\Common\Config\BaseApiConfig;

class BusinessBankingApiConfig extends BaseApiConfig
{
    const BALANCE_PATH = '/banking/v4/corporates/%s/accounts/%s';
    const STATEMENT_PATH = '/banking/v4/corporates/%s/accounts/%s/statements';
    const TRANSFER_PATH = '/banking/corporates/transfers';
    const ACCOUNT_VALIDATION_PATH = '/banking/general/corporates/%s/accounts/%s/validation';

    protected $corporateID;
    protected $balanceUri;
    protected $statementUri;
    protected $transferUri;
    protected $accountValidationUri;

    public function __construct(BusinessBankingApiConfigBuilder $builder)
    {
        parent::__construct($builder);
        $this->corporateID = $builder->getCorporateID();
        $this->balanceUri = $this->baseApiUri . static::BALANCE_PATH;
        $this->statementUri = $this->baseApiUri . static::STATEMENT_PATH;
        $this->transferUri = $this->baseApiUri . static::TRANSFER_PATH;
        $this->accountValidationUri = $this->baseApiUri . static::ACCOUNT_VALIDATION_PATH;
    }

    /**
     * @return string
     */
    public function getCorporateID()
    {
        return $this->corporateID;
    }

    /**
     * @return string
     */
    public function getBalanceUri()
    {
        return $this->balanceUri;
    }

    /**
     * @return string
     */
    public function getStatementUri()
    {
        return $this->statementUri;
    }

    /**
     * @return string
     */
    public function getTransferUri()
    {
        return $this->transferUri;
    }

    /**
     * @return string
     */
    public function getAccountValidationUri()
    {
        return $this->accountValidationUri;
    }
}