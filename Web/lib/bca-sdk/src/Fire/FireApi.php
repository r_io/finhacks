<?php
namespace Bca\Api\Sdk\Fire;

use Bca\Api\Sdk\Common\Http\DefaultHttpClient;
use Bca\Api\Sdk\Common\Http\HttpClient;
use Bca\Api\Sdk\Common\Http\RequestHelper;
use Bca\Api\Sdk\Common\OAuth2\Grant\Client\ClientCredentialsOAuth2Client;
use Bca\Api\Sdk\Common\OAuth2\Grant\Client\ClientCredentialsOAuth2Details;
use Bca\Api\Sdk\Common\Utils\DefaultJsonParser;
use Bca\Api\Sdk\Common\Utils\Logger\LoggerFactory;
use Bca\Api\Sdk\Fire\FireApiConfig;
use Bca\Api\Sdk\Fire\Models\Requests\AccountInquiryPayload;
use Bca\Api\Sdk\Fire\Models\Requests\BalanceInquiryPayload;
use Bca\Api\Sdk\Fire\Models\Requests\FireAuthenticationPayload;
use Bca\Api\Sdk\Fire\Models\Requests\TransactionInquiryPayload;
use Bca\Api\Sdk\Fire\Models\Requests\TransferToAccountPayload;
use Bca\Api\Sdk\Fire\Models\Responses\AccountInquiryResponse;
use Bca\Api\Sdk\Fire\Models\Responses\BalanceInquiryResponse;
use Bca\Api\Sdk\Fire\Models\Responses\TransactionInquiryResponse;
use Bca\Api\Sdk\Fire\Models\Responses\TransferToAccountResponse;

class FireApi
{
    use \Bca\Api\Sdk\Common\Utils\HasGetClass;
    use \Bca\Api\Sdk\Common\Utils\HasLogger;

    protected $config;
    protected $helper;

    public function __construct(FireApiConfig $config)
    {
        static::getLogger()->info('Configure Fire Api');
        static::getLogger()->debug('FireApi configs: ' . $config);

        $this->config = $config;

        $httpClient = new DefaultHttpClient();
        $details = new ClientCredentialsOAuth2Details($config);
        $oAuth2Client = new ClientCredentialsOAuth2Client($details, $httpClient);

        $this->helper = new RequestHelper($config, $oAuth2Client, $httpClient, new DefaultJsonParser());
    }

    public function setHttpClient(HttpClient $httpClient)
    {
        $details = new ClientCredentialsOAuth2Details($this->config);
        $oAuth2Client = new ClientCredentialsOAuth2Client($details, $httpClient);

        $this->helper = new RequestHelper($this->config, $oAuth2Client, $httpClient, new DefaultJsonParser());
    }

    public function transferToAccount(TransferToAccountPayload $payload)
    {
        static::getLogger()->info('Handle transferToAccount');

        $this->addAuthentication($payload);
        $url = $this->config->getTransferToAccountUri();
        return $this->helper->fetchAndWrapResponse(
            'POST',
            $url,
            $payload,
            TransferToAccountResponse::getClass()
        );
    }

    public function accountInquiry(AccountInquiryPayload $payload)
    {
        static::getLogger()->info('Handle accountInquiry');

        $this->addAuthentication($payload);
        $url = $this->config->getAccountInquiryUri();
        return $this->helper->fetchAndWrapResponse(
            'POST',
            $url,
            $payload,
            AccountInquiryResponse::getClass()
        );
    }

    public function balanceInquiry(BalanceInquiryPayload $payload)
    {
        static::getLogger()->info('Handle balanceInquiry');

        $this->addAuthentication($payload);
        $url = $this->config->getBalanceInquiryUri();
        return $this->helper->fetchAndWrapResponse(
            'POST',
            $url,
            $payload,
            BalanceInquiryResponse::getClass()
        );
    }

    public function transactionInquiry(TransactionInquiryPayload $payload)
    {
        static::getLogger()->info('Handle transactionInquiry');

        $this->addAuthentication($payload);
        $url = $this->config->getTransactionInquiryUri();
        return $this->helper->fetchAndWrapResponse(
            'POST',
            $url,
            $payload,
            TransactionInquiryResponse::getClass()
        );
    }

    protected function addAuthentication($payload)
    {
        $authentication = new FireAuthenticationPayload();
        $authentication->setCorporateID($this->config->getCorporateID());
        $authentication->setAccessCode($this->config->getAccessCode());
        $authentication->setBranchCode($this->config->getBranchCode());
        $authentication->setUserID($this->config->getUserID());
        $authentication->setLocalID($this->config->getLocalID());
        $payload->setAuthentication($authentication);
    }
}