<?php
namespace Bca\Api\Sdk\Fire;

use Bca\Api\Sdk\Common\Config\BaseApiConfig;

class FireApiConfig extends BaseApiConfig
{
    const BALANCE_INQUIRY_PATH = '/fire/accounts/balance';
    const ACCOUNT_INQUIRY_PATH = '/fire/accounts';
    const TRANSACTION_INQUIRY_PATH = '/fire/transactions';
    const TRANSFER_TO_ACCOUNT_PATH = '/fire/transactions/to-account';

    protected $corporateID;
    protected $accessCode;
    protected $branchCode;
    protected $userID;
    protected $localID;
    protected $balanceInquiryUri;
    protected $accountInquiryUri;
    protected $transactionInquiryUri;
    protected $transferToAccountUri;

    public function __construct(FireApiConfigBuilder $builder)
    {
        parent::__construct($builder);
        $this->corporateID = $builder->getCorporateID();
        $this->accessCode = $builder->getAccessCode();
        $this->branchCode = $builder->getBranchCode();
        $this->userID = $builder->getUserID();
        $this->localID = $builder->getLocalID();
        $this->balanceInquiryUri = $this->baseApiUri . static::BALANCE_INQUIRY_PATH;
        $this->accountInquiryUri = $this->baseApiUri . static::ACCOUNT_INQUIRY_PATH;
        $this->transactionInquiryUri = $this->baseApiUri . static::TRANSACTION_INQUIRY_PATH;
        $this->transferToAccountUri = $this->baseApiUri . static::TRANSFER_TO_ACCOUNT_PATH;
    }

    public function getCorporateID()
    {
        return $this->corporateID;
    }

    public function getAccessCode()
    {
        return $this->accessCode;
    }

    public function getBranchCode()
    {
        return $this->branchCode;
    }

    public function getUserID()
    {
        return $this->userID;
    }

    public function getLocalID()
    {
        return $this->localID;
    }

    public function getBalanceInquiryUri()
    {
        return $this->balanceInquiryUri;
    }

    public function getAccountInquiryUri()
    {
        return $this->accountInquiryUri;
    }

    public function getTransactionInquiryUri()
    {
        return $this->transactionInquiryUri;
    }

    public function getTransferToAccountUri()
    {
        return $this->transferToAccountUri;
    }
}