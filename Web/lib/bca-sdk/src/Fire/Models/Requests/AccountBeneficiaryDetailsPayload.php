<?php
namespace Bca\Api\Sdk\Fire\Models\Requests;

use Bca\Api\Sdk\Common\Utils\JsonSerializablePayload;

class AccountBeneficiaryDetailsPayload extends JsonSerializablePayload
{
    protected $bankCodeType;
    protected $bankCodeValue;
    protected $accountNumber;

    public function getBankCodeType()
    {
        return $this->bankCodeType;
    }

    public function setBankCodeType($bankCodeType)
    {
        $this->bankCodeType = (string)$bankCodeType;
    }

    public function getBankCodeValue()
    {
        return $this->bankCodeValue;
    }

    public function setBankCodeValue($bankCodeValue)
    {
        $this->bankCodeValue = (string)$bankCodeValue;
    }

    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = (string)$accountNumber;
    }
}