<?php
namespace Bca\Api\Sdk\Fire\Models\Requests;

use Bca\Api\Sdk\Common\Utils\JsonSerializablePayload;

class TransactionInquiryPayload extends JsonSerializablePayload
{
    protected $authentication;
    protected $transactionDetails;

    public function getAuthentication()
    {
        return $this->authentication;
    }

    public function setAuthentication(FireAuthenticationPayload $authentication)
    {
        $this->authentication = $authentication;
    }

    public function getTransactionDetails()
    {
        return $this->transactionDetails;
    }

    public function setTransactionDetails(TransactionDetailsPayload $transactionDetails)
    {
        $this->transactionDetails = $transactionDetails;
    }
}