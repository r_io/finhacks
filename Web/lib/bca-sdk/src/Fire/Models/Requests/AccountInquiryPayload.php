<?php
namespace Bca\Api\Sdk\Fire\Models\Requests;

use Bca\Api\Sdk\Common\Utils\JsonSerializablePayload;

class AccountInquiryPayload extends JsonSerializablePayload
{
    protected $authentication;
    protected $beneficiaryDetails;

    public function getAuthentication()
    {
        return $this->authentication;
    }

    public function setAuthentication(FireAuthenticationPayload $authentication)
    {
        $this->authentication = $authentication;
    }

    public function getBeneficiaryDetails()
    {
        return $this->beneficiaryDetails;
    }

    public function setBeneficiaryDetails(AccountBeneficiaryDetailsPayload $beneficiaryDetails)
    {
        $this->beneficiaryDetails = $beneficiaryDetails;
    }
}