<?php
namespace Bca\Api\Sdk\Fire\Models\Requests;

use Bca\Api\Sdk\Common\Utils\JsonSerializablePayload;

class TransferToAccountPayload extends JsonSerializablePayload
{
    protected $authentication;
    protected $senderDetails;
    protected $beneficiaryDetails;
    protected $transactionDetails;

    public function getAuthentication()
    {
        return $this->authentication;
    }

    public function setAuthentication(FireAuthenticationPayload $authentication)
    {
        $this->authentication = $authentication;
    }

    public function getSenderDetails()
    {
        return $this->senderDetails;
    }

    public function setSenderDetails(TransferSenderDetailsPayload $senderDetails)
    {
        $this->senderDetails = $senderDetails;
    }

    public function getBeneficiaryDetails()
    {
        return $this->beneficiaryDetails;
    }

    public function setBeneficiaryDetails(TransferBeneficiaryDetailsPayload $beneficiaryDetails)
    {
        $this->beneficiaryDetails = $beneficiaryDetails;
    }

    public function getTransactionDetails()
    {
        return $this->transactionDetails;
    }

    public function setTransactionDetails(TransferTransactionDetailsPayload $transactionDetails)
    {
        $this->transactionDetails = $transactionDetails;
    }
}