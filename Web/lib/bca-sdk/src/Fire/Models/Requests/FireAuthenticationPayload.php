<?php
namespace Bca\Api\Sdk\Fire\Models\Requests;

use Bca\Api\Sdk\Common\Utils\JsonSerializablePayload;

class FireAuthenticationPayload extends JsonSerializablePayload
{
    protected $corporateID;
    protected $accessCode;
    protected $branchCode;
    protected $userID;
    protected $localID;

    public function getCorporateID()
    {
        return $this->corporateID;
    }

    public function setCorporateID($corporateID)
    {
        $this->corporateID = (string)$corporateID;
    }

    public function getAccessCode()
    {
        return $this->accessCode;
    }

    public function setAccessCode($accessCode)
    {
        $this->accessCode = (string)$accessCode;
    }

    public function getBranchCode()
    {
        return $this->branchCode;
    }

    public function setBranchCode($branchCode)
    {
        $this->branchCode = (string)$branchCode;
    }

    public function getUserID()
    {
        return $this->userID;
    }

    public function setUserID($userID)
    {
        $this->userID = (string)$userID;
    }

    public function getLocalID()
    {
        return $this->localID;
    }

    public function setLocalID($localID)
    {
        $this->localID = (string)$localID;
    }
}