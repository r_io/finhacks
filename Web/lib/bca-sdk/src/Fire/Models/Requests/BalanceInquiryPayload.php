<?php
namespace Bca\Api\Sdk\Fire\Models\Requests;

use Bca\Api\Sdk\Common\Utils\JsonSerializablePayload;

class BalanceInquiryPayload extends JsonSerializablePayload
{
    protected $authentication;
    protected $FIDetails;

    public function getAuthentication()
    {
        return $this->authentication;
    }

    public function setAuthentication(FireAuthenticationPayload $authentication)
    {
        $this->authentication = $authentication;
    }

    public function getFIDetails()
    {
        return $this->FIDetails;
    }

    public function setFIDetails(BalanceFIDetailsPayload $FIDetails)
    {
        $this->FIDetails = $FIDetails;
    }
}