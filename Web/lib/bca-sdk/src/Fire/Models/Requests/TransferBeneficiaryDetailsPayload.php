<?php
namespace Bca\Api\Sdk\Fire\Models\Requests;

use Bca\Api\Sdk\Common\Utils\JsonSerializablePayload;

class TransferBeneficiaryDetailsPayload extends JsonSerializablePayload
{
    protected $name;
    protected $dateOfBirth;
    protected $address1;
    protected $address2;
    protected $city;
    protected $stateID;
    protected $postalCode;
    protected $countryID;
    protected $mobile;
    protected $identificationType;
    protected $identificationNumber;
    protected $nationalityID;
    protected $occupation;
    protected $bankCodeType;
    protected $bankCodeValue;
    protected $bankCountryID;
    protected $bankAddress;
    protected $bankCity;
    protected $accountNumber;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = (string)$name;
    }

    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = (string)$dateOfBirth;
    }

    public function getAddress1()
    {
        return $this->address1;
    }

    public function setAddress1($address1)
    {
        $this->address1 = (string)$address1;
    }

    public function getAddress2()
    {
        return $this->address2;
    }

    public function setAddress2($address2)
    {
        $this->address2 = (string)$address2;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCity($city)
    {
        $this->city = (string)$city;
    }

    public function getStateID()
    {
        return $this->stateID;
    }

    public function setStateID($stateID)
    {
        $this->stateID = (string)$stateID;
    }

    public function getPostalCode()
    {
        return $this->postalCode;
    }

    public function setPostalCode($postalCode)
    {
        $this->postalCode = (string)$postalCode;
    }

    public function getCountryID()
    {
        return $this->countryID;
    }

    public function setCountryID($countryID)
    {
        $this->countryID = (string)$countryID;
    }

    public function getMobile()
    {
        return $this->mobile;
    }

    public function setMobile($mobile)
    {
        $this->mobile = (string)$mobile;
    }

    public function getIdentificationType()
    {
        return $this->identificationType;
    }

    public function setIdentificationType($identificationType)
    {
        $this->identificationType = (string)$identificationType;
    }

    public function getIdentificationNumber()
    {
        return $this->identificationNumber;
    }

    public function setIdentificationNumber($identificationNumber)
    {
        $this->identificationNumber = (string)$identificationNumber;
    }

    public function getNationalityID()
    {
        return $this->nationalityID;
    }

    public function setNationalityID($nationalityID)
    {
        $this->nationalityID = (string)$nationalityID;
    }

    public function getOccupation()
    {
        return $this->occupation;
    }

    public function setOccupation($occupation)
    {
        $this->occupation = (string)$occupation;
    }

    public function getBankCodeType()
    {
        return $this->bankCodeType;
    }

    public function setBankCodeType($bankCodeType)
    {
        $this->bankCodeType = (string)$bankCodeType;
    }

    public function getBankCodeValue()
    {
        return $this->bankCodeValue;
    }

    public function setBankCodeValue($bankCodeValue)
    {
        $this->bankCodeValue = (string)$bankCodeValue;
    }

    public function getBankCountryID()
    {
        return $this->bankCountryID;
    }

    public function setBankCountryID($bankCountryID)
    {
        $this->bankCountryID = (string)$bankCountryID;
    }

    public function getBankAddress()
    {
        return $this->bankAddress;
    }

    public function setBankAddress($bankAddress)
    {
        $this->bankAddress = (string)$bankAddress;
    }

    public function getBankCity()
    {
        return $this->bankCity;
    }

    public function setBankCity($bankCity)
    {
        $this->bankCity = (string)$bankCity;
    }

    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = (string)$accountNumber;
    }
}