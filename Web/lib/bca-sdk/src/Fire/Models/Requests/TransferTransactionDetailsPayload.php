<?php
namespace Bca\Api\Sdk\Fire\Models\Requests;

use Bca\Api\Sdk\Common\Utils\JsonSerializablePayload;

class TransferTransactionDetailsPayload extends JsonSerializablePayload
{
    protected $currencyID;
    protected $amount;
    protected $purposeCode;
    protected $description1;
    protected $description2;
    protected $detailOfCharges;
    protected $sourceOfFund;
    protected $formNumber;

    public function getCurrencyID()
    {
        return $this->currencyID;
    }

    public function setCurrencyID($currencyID)
    {
        $this->currencyID = (string)$currencyID;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setAmount($amount)
    {
        $this->amount = (string)$amount;
    }

    public function getPurposeCode()
    {
        return $this->purposeCode;
    }

    public function setPurposeCode($purposeCode)
    {
        $this->purposeCode = (string)$purposeCode;
    }

    public function getDescription1()
    {
        return $this->description1;
    }

    public function setDescription1($description1)
    {
        $this->description1 = (string)$description1;
    }

    public function getDescription2()
    {
        return $this->description2;
    }

    public function setDescription2($description2)
    {
        $this->description2 = (string)$description2;
    }

    public function getDetailOfCharges()
    {
        return $this->detailOfCharges;
    }

    public function setDetailOfCharges($detailOfCharges)
    {
        $this->detailOfCharges = (string)$detailOfCharges;
    }

    public function getSourceOfFund()
    {
        return $this->sourceOfFund;
    }

    public function setSourceOfFund($sourceOfFund)
    {
        $this->sourceOfFund = (string)$sourceOfFund;
    }

    public function getFormNumber()
    {
        return $this->formNumber;
    }

    public function setFormNumber($formNumber)
    {
        $this->formNumber = (string)$formNumber;
    }
}