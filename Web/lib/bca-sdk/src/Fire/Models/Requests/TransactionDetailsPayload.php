<?php
namespace Bca\Api\Sdk\Fire\Models\Requests;

use Bca\Api\Sdk\Common\Utils\JsonSerializablePayload;

class TransactionDetailsPayload extends JsonSerializablePayload
{
    protected $inquiryBy;
    protected $inquiryValue;

    public function getInquiryBy()
    {
        return $this->inquiryBy;
    }

    public function setInquiryBy($inquiryBy)
    {
        $this->inquiryBy = (string)$inquiryBy;
    }

    public function getInquiryValue()
    {
        return $this->inquiryValue;
    }

    public function setInquiryValue($inquiryValue)
    {
        $this->inquiryValue = (string)$inquiryValue;
    }
}