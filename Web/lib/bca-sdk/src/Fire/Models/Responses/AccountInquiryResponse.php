<?php
namespace Bca\Api\Sdk\Fire\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class AccountInquiryResponse extends JsonUnserializableResponse
{
    public static function fromJson($json)
    {
        $result = parent::fromJson($json);
        $result->beneficiaryDetails = AccountBeneficiaryDetails::fromJson(json_encode($result->beneficiaryDetails));
        return $result;
    }

    protected $statusTransaction;
    protected $statusMessage;
    protected $beneficiaryDetails;

    public function getStatusTransaction()
    {
        return $this->statusTransaction;
    }

    public function getStatusMessage()
    {
        return $this->statusMessage;
    }

    public function getBeneficiaryDetails()
    {
        return $this->beneficiaryDetails;
    }
}