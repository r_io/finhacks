<?php
namespace Bca\Api\Sdk\Fire\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class TransactionSenderDetails extends JsonUnserializableResponse
{
    protected $firstName;
    protected $lastName;

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }
}