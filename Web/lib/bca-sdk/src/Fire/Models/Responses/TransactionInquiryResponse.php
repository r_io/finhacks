<?php
namespace Bca\Api\Sdk\Fire\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class TransactionInquiryResponse extends JsonUnserializableResponse
{
    public static function fromJson($json)
    {
        $result = parent::fromJson($json);
        $result->senderDetails = TransactionSenderDetails::fromJson(json_encode($result->senderDetails));
        $result->beneficiaryDetails = TransactionBeneficiaryDetails::fromJson(json_encode($result->beneficiaryDetails));
        $result->transactionDetails = TransactionDetails::fromJson(json_encode($result->transactionDetails));
        return $result;
    }

    protected $statusTransaction;
    protected $statusMessage;
    protected $senderDetails;
    protected $beneficiaryDetails;
    protected $transactionDetails;

    public function getStatusTransaction()
    {
        return $this->statusTransaction;
    }

    public function getStatusMessage()
    {
        return $this->statusMessage;
    }

    public function getSenderDetails()
    {
        return $this->senderDetails;
    }

    public function getBeneficiaryDetails()
    {
        return $this->beneficiaryDetails;
    }

    public function getTransactionDetails()
    {
        return $this->transactionDetails;
    }
}