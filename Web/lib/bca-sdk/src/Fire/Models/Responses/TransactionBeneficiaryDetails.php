<?php
namespace Bca\Api\Sdk\Fire\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class TransactionBeneficiaryDetails extends JsonUnserializableResponse
{
    protected $name;
    protected $bankCodeType;
    protected $bankCodeValue;
    protected $accountNumber;

    public function getName()
    {
        return $this->name;
    }

    public function getBankCodeType()
    {
        return $this->bankCodeType;
    }

    public function getBankCodeValue()
    {
        return $this->bankCodeValue;
    }

    public function getAccountNumber()
    {
        return $this->accountNumber;
    }
}