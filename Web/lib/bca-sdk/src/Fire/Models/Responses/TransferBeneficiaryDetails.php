<?php
namespace Bca\Api\Sdk\Fire\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class TransferBeneficiaryDetails extends JsonUnserializableResponse
{
    protected $name;
    protected $accountNumber;
    protected $serverBeneAccountName;

    public function getName()
    {
        return $this->name;
    }

    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    public function getServerBeneAccountName()
    {
        return $this->serverBeneAccountName;
    }
}