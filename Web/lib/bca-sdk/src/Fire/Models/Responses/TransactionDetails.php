<?php
namespace Bca\Api\Sdk\Fire\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class TransactionDetails extends JsonUnserializableResponse
{
    protected $amountPaid;
    protected $currencyID;
    protected $releaseDateTime;
    protected $localID;
    protected $formNumber;
    protected $referenceNumber;
    protected $PIN;
    protected $description1;
    protected $description2;

    public function getAmountPaid()
    {
        return $this->amountPaid;
    }

    public function getCurrencyID()
    {
        return $this->currencyID;
    }

    public function getReleaseDateTime()
    {
        return $this->releaseDateTime;
    }

    public function getLocalID()
    {
        return $this->localID;
    }

    public function getFormNumber()
    {
        return $this->formNumber;
    }

    public function getReferenceNumber()
    {
        return $this->referenceNumber;
    }

    public function getPIN()
    {
        return $this->PIN;
    }

    public function getDescription1()
    {
        return $this->description1;
    }

    public function getDescription2()
    {
        return $this->description2;
    }
}