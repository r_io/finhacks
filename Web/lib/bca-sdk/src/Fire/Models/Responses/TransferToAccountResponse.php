<?php
namespace Bca\Api\Sdk\Fire\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class TransferToAccountResponse extends JsonUnserializableResponse
{
    public static function fromJson($json)
    {
        $result = parent::fromJson($json);
        $result->beneficiaryDetails = TransferBeneficiaryDetails::fromJson(json_encode($result->beneficiaryDetails));
        $result->transactionDetails = TransferTransactionDetails::fromJson(json_encode($result->transactionDetails));
        return $result;
    }

    protected $statusTransaction;
    protected $statusMessage;
    protected $beneficiaryDetails;
    protected $transactionDetails;

    public function getStatusTransaction()
    {
        return $this->statusTransaction;
    }

    public function getStatusMessage()
    {
        return $this->statusMessage;
    }

    public function getBeneficiaryDetails()
    {
        return $this->beneficiaryDetails;
    }

    public function getTransactionDetails()
    {
        return $this->transactionDetails;
    }
}