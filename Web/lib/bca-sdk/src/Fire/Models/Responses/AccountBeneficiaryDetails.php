<?php
namespace Bca\Api\Sdk\Fire\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class AccountBeneficiaryDetails extends JsonUnserializableResponse
{
    protected $serverBeneAccountName;

    public function getServerBeneAccountName()
    {
        return $this->serverBeneAccountName;
    }
}