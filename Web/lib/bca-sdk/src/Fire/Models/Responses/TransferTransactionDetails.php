<?php
namespace Bca\Api\Sdk\Fire\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class TransferTransactionDetails extends JsonUnserializableResponse
{
    protected $currencyID;
    protected $purposeCode;
    protected $amount;
    protected $description1;
    protected $description2;
    protected $formNumber;
    protected $referenceNumber;
    protected $releaseDateTime;

    public function getCurrencyID()
    {
        return $this->currencyID;
    }

    public function getPurposeCode()
    {
        return $this->purposeCode;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getDescription1()
    {
        return $this->description1;
    }

    public function getDescription2()
    {
        return $this->description2;
    }

    public function getFormNumber()
    {
        return $this->formNumber;
    }

    public function getReferenceNumber()
    {
        return $this->referenceNumber;
    }

    public function getReleaseDateTime()
    {
        return $this->releaseDateTime;
    }
}