<?php
namespace Bca\Api\Sdk\Fire\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class BalanceInquiryResponse extends JsonUnserializableResponse
{
    public static function fromJson($json)
    {
        $result = parent::fromJson($json);
        $result->FIDetails = BalanceFIDetails::fromJson(json_encode($result->FIDetails));
        return $result;
    }

    protected $statusTransaction;
    protected $statusMessage;
    protected $FIDetails;

    public function getStatusTransaction()
    {
        return $this->statusTransaction;
    }

    public function getStatusMessage()
    {
        return $this->statusMessage;
    }

    public function getFIDetails()
    {
        return $this->FIDetails;
    }
}