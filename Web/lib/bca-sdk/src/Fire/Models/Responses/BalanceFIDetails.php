<?php
namespace Bca\Api\Sdk\Fire\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class BalanceFIDetails extends JsonUnserializableResponse
{
    protected $currencyID;
    protected $accountBalance;

    public function getCurrencyID()
    {
        return $this->currencyID;
    }

    public function getAccountBalance()
    {
        return $this->accountBalance;
    }
}