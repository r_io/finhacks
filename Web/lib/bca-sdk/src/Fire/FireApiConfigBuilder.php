<?php
namespace Bca\Api\Sdk\Fire;

use Bca\Api\Sdk\Common\Config\BaseApiConfigBuilder;
use Bca\Api\Sdk\Common\Utils\Validator;

class FireApiConfigBuilder extends BaseApiConfigBuilder
{
    protected $corporateID;
    protected $accessCode;
    protected $branchCode;
    protected $userID;
    protected $localID;

    public function getCorporateID()
    {
        return $this->corporateID;
    }

    public function corporateID($corporateID)
    {
        $this->corporateID = (string)$corporateID;
        return $this;
    }

    public function getAccessCode()
    {
        return $this->accessCode;
    }

    public function accessCode($accessCode)
    {
        $this->accessCode = (string)$accessCode;
        return $this;
    }

    public function getBranchCode()
    {
        return $this->branchCode;
    }

    public function branchCode($branchCode)
    {
        $this->branchCode = (string)$branchCode;
        return $this;
    }

    public function getUserID()
    {
        return $this->userID;
    }

    public function userID($userID)
    {
        $this->userID = (string)$userID;
        return $this;
    }

    public function getLocalID()
    {
        return $this->localID;
    }

    public function localID($localID)
    {
        $this->localID = (string)$localID;
        return $this;
    }

    public function build()
    {
        $this->validateProperties();
        return new FireApiConfig($this);
    }

    protected function validateProperties()
    {
        parent::validateProperties();
        Validator::shouldNotNullOrEmpty($this->corporateID);
        Validator::shouldNotNullOrEmpty($this->accessCode);
        Validator::shouldNotNullOrEmpty($this->branchCode);
        Validator::shouldNotNullOrEmpty($this->userID);
        Validator::shouldNotNullOrEmpty($this->localID);
    }
}