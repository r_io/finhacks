<?php
namespace Bca\Api\Sdk\General;

use Bca\Api\Sdk\Common\Http\DefaultHttpClient;
use Bca\Api\Sdk\Common\Http\HttpClient;
use Bca\Api\Sdk\Common\Http\RequestHelper;
use Bca\Api\Sdk\Common\OAuth2\Grant\Client\ClientCredentialsOAuth2Client;
use Bca\Api\Sdk\Common\OAuth2\Grant\Client\ClientCredentialsOAuth2Details;
use Bca\Api\Sdk\Common\Utils\DefaultJsonParser;
use Bca\Api\Sdk\Common\Utils\Logger\LoggerFactory;
use Bca\Api\Sdk\General\GeneralApiConfig;
use Bca\Api\Sdk\General\Models\Requests\AtmParams;
use Bca\Api\Sdk\General\Models\Requests\ForexParams;
use Bca\Api\Sdk\General\Models\Responses\AtmResponse;
use Bca\Api\Sdk\General\Models\Responses\DepositResponse;
use Bca\Api\Sdk\General\Models\Responses\ForexResponse;

class GeneralApi
{
    use \Bca\Api\Sdk\Common\Utils\HasGetClass;
    use \Bca\Api\Sdk\Common\Utils\HasLogger;

    protected $config;
    protected $helper;

    public function __construct(GeneralApiConfig $config)
    {
        static::getLogger()->info('Configure General Api');
        static::getLogger()->debug('GeneralApi configs: ' . $config);

        $this->config = $config;

        $httpClient = new DefaultHttpClient();
        $details = new ClientCredentialsOAuth2Details($config);
        $oAuth2Client = new ClientCredentialsOAuth2Client($details, $httpClient);

        $this->helper = new RequestHelper($config, $oAuth2Client, $httpClient, new DefaultJsonParser());
    }

    public function setHttpClient(HttpClient $httpClient)
    {
        $details = new ClientCredentialsOAuth2Details($this->config);
        $oAuth2Client = new ClientCredentialsOAuth2Client($details, $httpClient);

        $this->helper = new RequestHelper($this->config, $oAuth2Client, $httpClient, new DefaultJsonParser());
    }

    public function getForexRate(ForexParams $params)
    {
        static::getLogger()->info('Handle getForexRate');

        $url = $this->config->getForexUri() .
            '?' . (string)$params;
        return $this->helper->fetchAndWrapResponse(
            'GET',
            $url,
            '',
            ForexResponse::getClass()
        );
    }

    public function getDepositRate()
    {
        static::getLogger()->info('Handle getDepositRate');

        $url = $this->config->getDepositUri();
        return $this->helper->fetchAndWrapResponse(
            'GET',
            $url,
            '',
            DepositResponse::getClass()
        );
    }

    public function getAtmLocations(AtmParams $params)
    {
        static::getLogger()->info('Handle getAtmLocations');

        $url = $this->config->getAtmUri() .
            '?' . (string)$params;
        return $this->helper->fetchAndWrapResponse(
            'GET',
            $url,
            '',
            AtmResponse::getClass()
        );
    }
}