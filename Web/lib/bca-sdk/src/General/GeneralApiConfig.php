<?php
namespace Bca\Api\Sdk\General;

use Bca\Api\Sdk\Common\Config\BaseApiConfig;
use Bca\Api\Sdk\General\GeneralApiConfigBuilder;

class GeneralApiConfig extends BaseApiConfig
{
    const FOREX_PATH = '/general/rate/forex/';
    const DEPOSIT_PATH = '/general/rate/deposit/';
    const ATM_PATH = '/general/info-bca/atm/';

    protected $forexUri;
    protected $depositUri;
    protected $atmUri;

    public function __construct(GeneralApiConfigBuilder $builder)
    {
        parent::__construct($builder);
        $this->forexUri = $this->baseApiUri . static::FOREX_PATH;
        $this->depositUri = $this->baseApiUri . static::DEPOSIT_PATH;
        $this->atmUri = $this->baseApiUri . static::ATM_PATH;
    }

    public function getForexUri()
    {
        return $this->forexUri;
    }

    public function getDepositUri()
    {
        return $this->depositUri;
    }

    public function getAtmUri()
    {
        return $this->atmUri;
    }
}