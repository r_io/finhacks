<?php
namespace Bca\Api\Sdk\General;

use Bca\Api\Sdk\Common\Config\BaseApiConfigBuilder;

class GeneralApiConfigBuilder extends BaseApiConfigBuilder
{
    /**
     * @return GeneralApiConfig
     * @throws InvalidArgumentException
     */
    public function build()
    {
        $this->validateProperties();
        return new GeneralApiConfig($this);
    }
}