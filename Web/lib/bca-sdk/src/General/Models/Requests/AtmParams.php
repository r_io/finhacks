<?php
namespace Bca\Api\Sdk\General\Models\Requests;

use Bca\Api\Sdk\Common\Utils\Formatter;
use Bca\Api\Sdk\Common\Utils\JsonSerializablePayload;

class AtmParams extends JsonSerializablePayload
{
    const SEARCH_BY = 'SearchBy';
    const LATITUDE = 'Latitude';
    const LONGITUDE = 'Longitude';
    const COUNT = 'Count';
    const RADIUS = 'Radius';

    protected $searchBy;
    protected $latitude;
    protected $longitude;
    protected $count;
    protected $radius;

    public function __toString()
    {
        return Formatter::httpQuery([
            static::SEARCH_BY => $this->searchBy,
            static::LATITUDE => $this->latitude,
            static::LONGITUDE => $this->longitude,
            static::COUNT => $this->count,
            static::RADIUS => $this->radius,
        ]);
    }

    /**
     * @return string
     */
    public function getSearchBy()
    {
        return $this->searchBy;
    }

    /**
     * @param string $searchBy
     */
    public function setSearchBy($searchBy)
    {
        $this->searchBy = (string)$searchBy;
    }

    /**
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param string $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = (string)$latitude;
    }

    /**
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param string $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = (string)$longitude;
    }

    /**
     * @return string
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param string $count
     */
    public function setCount($count)
    {
        $this->count = (string)$count;
    }

    /**
     * @return string
     */
    public function getRadius()
    {
        return $this->radius;
    }

    /**
     * @param string $radius
     */
    public function setRadius($radius)
    {
        $this->radius = (string)$radius;
    }
}