<?php
namespace Bca\Api\Sdk\General\Models\Requests;

use Bca\Api\Sdk\Common\Utils\Formatter;

class ForexParams
{
    const CURRENCY_CODE = 'CurrencyCode';
    const RATE_TYPE = 'RateType';

    protected $currencyCode;
    protected $rateType;

    public function __toString()
    {
        return Formatter::httpQuery([
            static::CURRENCY_CODE => $this->currencyCode,
            static::RATE_TYPE => $this->rateType,
        ]);
    }

    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = (string)$currencyCode;
    }

    public function getRateType()
    {
        return $this->rateType;
    }

    public function setRateType($rateType)
    {
        $this->rateType = (string)$rateType;
    }
}