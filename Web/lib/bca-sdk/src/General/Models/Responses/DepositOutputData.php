<?php
namespace Bca\Api\Sdk\General\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class DepositOutputData extends JsonUnserializableResponse
{
    protected $productCode;
    protected $productName;
    protected $month01;
    protected $month03;
    protected $month06;
    protected $month12;

    /**
     * @return string
     */
    public function getProductCode()
    {
        return $this->productCode;
    }

    /**
     * @return string
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * @return string
     */
    public function getMonth01()
    {
        return $this->month01;
    }

    /**
     * @return string
     */
    public function getMonth03()
    {
        return $this->month03;
    }

    /**
     * @return string
     */
    public function getMonth06()
    {
        return $this->month06;
    }

    /**
     * @return string
     */
    public function getMonth12()
    {
        return $this->month12;
    }
}