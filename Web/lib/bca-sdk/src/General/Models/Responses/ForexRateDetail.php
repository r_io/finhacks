<?php
namespace Bca\Api\Sdk\General\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class ForexRateDetail extends JsonUnserializableResponse
{
    protected $rateType;
    protected $buy;
    protected $sell;
    protected $lastUpdate;

    public function getRateType()
    {
        return $this->rateType;
    }

    public function getBuy()
    {
        return $this->buy;
    }

    public function getSell()
    {
        return $this->sell;
    }

    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }
}