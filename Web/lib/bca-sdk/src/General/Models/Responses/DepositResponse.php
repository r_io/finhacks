<?php
namespace Bca\Api\Sdk\General\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class DepositResponse extends JsonUnserializableResponse
{
    public static function fromJson($json)
    {
        $result = parent::fromJson($json);
        $details = [];
        foreach ($result->outputData as $jsonDetail) {
            $details[] = DepositOutputData::fromJson(json_encode($jsonDetail));
        }
        $result->outputData = $details;
        return $result;
    }

    protected $date;
    protected $description1;
    protected $description2;
    protected $description3;
    protected $outputData;

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getDescription1()
    {
        return $this->description1;
    }

    /**
     * @return string
     */
    public function getDescription2()
    {
        return $this->description2;
    }

    /**
     * @return string
     */
    public function getDescription3()
    {
        return $this->description3;
    }

    /**
     * @return array
     */
    public function getOutputData()
    {
        return $this->outputData;
    }
}