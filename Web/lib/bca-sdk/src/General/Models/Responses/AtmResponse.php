<?php
namespace Bca\Api\Sdk\General\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class AtmResponse extends JsonUnserializableResponse
{
	public static function fromJson($json)
    {
        $result = parent::fromJson($json);
        $details = [];
        foreach ($result->ATMDetails as $jsonDetail) {
            $details[] = AtmDetail::fromJson(json_encode($jsonDetail));
        }
        $result->ATMDetails = $details;
        return $result;
    }

    protected $ATMDetails;

    /**
     * @return string
     */
    public function getATMDetails()
    {
        return $this->ATMDetails;
    }
}