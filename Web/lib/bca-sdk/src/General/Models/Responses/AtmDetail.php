<?php
namespace Bca\Api\Sdk\General\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class AtmDetail extends JsonUnserializableResponse
{
    protected $type;
    protected $address;
    protected $city;
    protected $country;
    protected $latitude;
    protected $longitude;
    protected $distance;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @return string
     */
    public function getDistance()
    {
        return $this->distance;
    }
}