<?php
namespace Bca\Api\Sdk\General\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class ForexResponse extends JsonUnserializableResponse
{
    public static function fromJson($json)
    {
        $result = parent::fromJson($json);
        $details = [];
        if ($result->currencies) foreach ($result->currencies as $jsonDetail) {
            $details[] = ForexCurrency::fromJson(json_encode($jsonDetail));
        }
        $result->currencies = $details;
        return $result;
    }

    protected $currencies;
    protected $invalidRateType;
    protected $invalidCurrencyCode;

    public function getCurrencies()
    {
        return $this->currencies;
    }

    public function getInvalidRateType()
    {
        return $this->invalidRateType;
    }

    public function getInvalidCurrencyCode()
    {
        return $this->invalidCurrencyCode;
    }
}