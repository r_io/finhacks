<?php
namespace Bca\Api\Sdk\General\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class ForexCurrency extends JsonUnserializableResponse
{
    public static function fromJson($json)
    {
        $result = parent::fromJson($json);
        $details = [];
        if ($result->rateDetail) foreach ($result->rateDetail as $jsonDetail) {
            $details[] = ForexRateDetail::fromJson(json_encode($jsonDetail));
        }
        $result->rateDetail = $details;
        return $result;
    }

    protected $currencyCode;
    protected $rateDetail;

    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    public function getRateDetail()
    {
        return $this->rateDetail;
    }
}