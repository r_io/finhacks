<?php
namespace Bca\Api\Sdk\Common\OAuth2;

class BaseOAuth2Details
{
    use \Bca\Api\Sdk\Common\Utils\HasGetClass;

    protected $clientId;
    protected $clientSecret;
    protected $accessTokenUri;
    protected $grantType;
    protected $scopes;

    /**
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param string $clientId
     */
    public function setClientId($clientId)
    {
        $this->clientId = (string)$clientId;
    }

    /**
     * @return string
     */
    public function getClientSecret()
    {
        return $this->clientSecret;
    }

    /**
     * @param string $clientSecret
     */
    public function setClientSecret($clientSecret)
    {
        $this->clientSecret = (string)$clientSecret;
    }

    /**
     * @return string
     */
    public function getAccessTokenUri()
    {
        return $this->accessTokenUri;
    }

    /**
     * @param string $accessTokenUri
     */
    public function setAccessTokenUri($accessTokenUri)
    {
        $this->accessTokenUri = (string)$accessTokenUri;
    }

    /**
     * @return string
     */
    public function getGrantType()
    {
        return $this->grantType;
    }

    /**
     * @param string $grantType
     */
    public function setGrantType($grantType)
    {
        $this->grantType = (string)$grantType;
    }

    /**
     * @return string[]
     */
    public function getScopes()
    {
        return $this->scopes;
    }

    /**
     * @param string[] $scopes
     */
    public function setScopes(array $scopes)
    {
        $this->scopes = $scopes;
    }
}