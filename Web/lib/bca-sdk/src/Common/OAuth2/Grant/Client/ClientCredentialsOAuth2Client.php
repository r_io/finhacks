<?php
namespace Bca\Api\Sdk\Common\OAuth2\Grant\Client;

use Bca\Api\Sdk\Common\Exceptions\OAuth2Exception;
use Bca\Api\Sdk\Common\Http\DefaultHttpClient;
use Bca\Api\Sdk\Common\Http\HeaderName;
use Bca\Api\Sdk\Common\Http\HttpClient;
use Bca\Api\Sdk\Common\Http\Models\HttpHeader;
use Bca\Api\Sdk\Common\OAuth2\OAuth2AccessToken;
use Bca\Api\Sdk\Common\OAuth2\OAuth2Client;
use Bca\Api\Sdk\Common\Utils\Logger\LoggerFactory;

class ClientCredentialsOAuth2Client implements OAuth2Client
{
    use \Bca\Api\Sdk\Common\Utils\HasLogger;

    protected $details;
    protected $oAuth2AccessToken;
    protected $http;

    public function __construct(ClientCredentialsOAuth2Details $details, HttpClient $http = null)
    {
        $this->details = $details;
        if ($http === null) {
            $this->http = new DefaultHttpClient();
        } else {
            $this->http = $http;
        }
    }

    /**
     * @return OAuth2AccessToken
     */
    public function getOAuth2AccessToken()
    {
        return $this->oAuth2AccessToken;
    }

    /**
     * @return string
     * @throws OAuth2Exception
     */
    public function getAccessToken()
    {
        static::getLogger()->debug("Request access token to {$this->details->getAccessTokenUri()}, clientId={$this->details->getClientId()}, clientSecret={$this->details->getClientSecret()}, grant=client_credentials");
        // @todo use real OAuth 2 client
        $response = $this->sendRequest();

        if ($response->getStatusCode() === '200') {
            $this->oAuth2AccessToken = $this->convertToAccessToken($response->getResponseBody());
            return $this->oAuth2AccessToken->getToken();
        } else {
            throw new OAuth2Exception($response->getResponseBody(), OAuth2Exception::INVALID_REQUEST);
        }
    }

    protected function sendRequest()
    {
        $url = $this->details->getAccessTokenUri();
        $clientId = $this->details->getClientId();
        $clientSecret = $this->details->getClientSecret();
        $headers = [
            new HttpHeader(HeaderName::AUTHORIZATION, 'Basic '.base64_encode("$clientId:$clientSecret")),
            new HttpHeader(HeaderName::CONTENT_TYPE, 'application/x-www-form-urlencoded'),
        ];
        $body = 'grant_type=client_credentials';
        $response = $this->http->post($url, $headers, $body);
        return $response;
    }

    protected function convertToAccessToken($body)
    {
        $decoded = json_decode($body);
        $token = new OAuth2AccessToken();
        $token->setToken($decoded->access_token);
        $token->setTokenType($decoded->token_type);
        $token->setExpireIn($decoded->expires_in);
        $token->setScopes(explode(' ', $decoded->scope));
        return $token;
    }
}