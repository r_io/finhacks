<?php
namespace Bca\Api\Sdk\Common\OAuth2\Grant\Client;

use Bca\Api\Sdk\Common\Config\BaseApiConfig;
use Bca\Api\Sdk\Common\OAuth2\BaseOAuth2Details;
use Bca\Api\Sdk\Common\OAuth2\GrantType;

class ClientCredentialsOAuth2Details extends BaseOAuth2Details
{
    public function __construct(BaseApiConfig $config)
    {
        $this->setClientId($config->getClientId());
        $this->setClientSecret($config->getClientSecret());
        $this->setScopes($config->getScopes());
        $this->setAccessTokenUri($config->getAccessTokenUri());
        $this->setGrantType(GrantType::CLIENT_CREDENTIALS);
    }
}