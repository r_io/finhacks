<?php
namespace Bca\Api\Sdk\Common\OAuth2;

interface OAuth2Client
{
    /**
     * @return string
     * @throws OAuth2Exception
     */
    public function getAccessToken();
}