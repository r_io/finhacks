<?php
namespace Bca\Api\Sdk\Common\OAuth2;

class GrantType
{
    const AUTHORIZATION_CODE = 'authorization_code';
    const CLIENT_CREDENTIALS = 'client_credentials';
    const PASSWORD = 'password';
}