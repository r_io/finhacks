<?php
namespace Bca\Api\Sdk\Common\OAuth2;

class OAuth2AccessToken
{
    use \Bca\Api\Sdk\Common\Utils\HasGetClass;

    protected $token;
    protected $tokenType;
    protected $refreshToken;
    protected $scopes;
    protected $expireIn;

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = (string)$token;
    }

    /**
     * @return string
     */
    public function getTokenType()
    {
        return $this->tokenType;
    }

    /**
     * @param string $tokenType
     */
    public function setTokenType($tokenType)
    {
        $this->tokenType = (string)$tokenType;
    }

    /**
     * @return string
     */
    public function getRefreshToken()
    {
        return $this->refreshToken;
    }

    /**
     * @param string $refreshToken
     */
    public function setRefreshToken($refreshToken)
    {
        $this->refreshToken = (string)$refreshToken;
    }

    /**
     * @return string[]
     */
    public function getScopes()
    {
        return $this->scopes;
    }

    /**
     * @param string[] $scopes
     */
    public function setScopes(array $scopes)
    {
        $this->scopes = $scopes;
    }

    /**
     * @return int
     */
    public function getExpireIn()
    {
        return $this->expireIn;
    }

    /**
     * @param int $expireIn
     */
    public function setExpireIn($expireIn)
    {
        $this->expireIn = (int)$expireIn;
    }

    /**
     * @return bool
     */
    public function isRefreshable()
    {
        // @todo
        return false;
    }
}