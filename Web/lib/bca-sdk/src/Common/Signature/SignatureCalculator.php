<?php
namespace Bca\Api\Sdk\Common\Signature;

use Bca\Api\Sdk\Common\Utils\Formatter;
use Bca\Api\Sdk\Common\Utils\Logger\LoggerFactory;

class SignatureCalculator
{
    use \Bca\Api\Sdk\Common\Utils\HasLogger;

    public static function sign(SignatureData $sign, $key)
    {
        static::getLogger()->debug('Signature data: ' . $sign);
        $signature = static::getSignature($key,
            $sign->getHttpMethod(),
            $sign->getRawRelativeUrl(),
            $sign->getAccessToken(),
            $sign->getRawRequestBody(),
            $sign->getTimestamp()
        );
        static::getLogger()->debug('Signature result = ' . $signature);
        return $signature;
    }

    protected static function getSignature($apiSecret, $httpMethod, $url, $accessToken, $body, $timestamp)
    {
        $stringToSign = static::getStringToSign($httpMethod, $url, $accessToken, $body, $timestamp);
        return hash_hmac('sha256', $stringToSign, $apiSecret);
    }

    protected static function getStringToSign($httpMethod, $url, $accessToken, $body, $timestamp)
    {
        $httpMethod = strtoupper($httpMethod);
        $relativeUrl = static::getRelativeUrl($url);
        $bodyHash = static::hashBody($body);
        $formattedTime = Formatter::ISO8601Time($timestamp);

        return "{$httpMethod}:{$relativeUrl}:{$accessToken}:{$bodyHash}:{$formattedTime}";
    }

    protected static function getRelativeUrl($url)
    {
        $path = parse_url($url, PHP_URL_PATH);
        if (empty($path)) {
            $path = '/';
        } else {
            $path = implode('/', array_map('rawurlencode', explode('/', $path)));
        }

        $query = parse_url($url, PHP_URL_QUERY);
        if ($query) {
            parse_str($query, $parsed);
            ksort($parsed);
            $query = '?' . Formatter::httpQuery($parsed);
        }

        return $path . $query;
    }

    protected static function hashBody($body)
    {
        if (empty($body)) {
            $body = '';
        } else {
            $toStrip = [" ", "\r", "\n", "\t"];
            $body = str_replace($toStrip, '', $body);
        }
        return strtolower(hash('sha256', $body));
    }
}