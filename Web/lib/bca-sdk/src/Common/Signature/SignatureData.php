<?php
namespace Bca\Api\Sdk\Common\Signature;

class SignatureData
{
    protected $httpMethod;
    protected $rawRelativeUrl;
    protected $accessToken;
    protected $rawRequestBody;
    protected $timestamp;

    public function __construct(SignatureDataBuilder $builder)
    {
        $this->httpMethod = $builder->getHttpMethod();
        $this->rawRelativeUrl = $builder->getRawRelativeUrl();
        $this->accessToken = $builder->getAccessToken();
        $this->rawRequestBody = $builder->getRawRequestBody();
        $this->timestamp = $builder->getTimestamp();
    }

    /**
     * @return string
     */
    public function getHttpMethod()
    {
        return $this->httpMethod;
    }

    /**
     * @return string
     */
    public function getRawRelativeUrl()
    {
        return $this->rawRelativeUrl;
    }

    /**
     * @return string
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @return string
     */
    public function getRawRequestBody()
    {
        return $this->rawRequestBody;
    }

    /**
     * @return string
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    public function __toString()
    {
        return "{httpMethod={$this->httpMethod}, rawRelativeUrl={$this->rawRelativeUrl}, accessToken={$this->accessToken}, rawRequestBody={$this->rawRequestBody}, timestamp={$this->timestamp}}";
    }
}