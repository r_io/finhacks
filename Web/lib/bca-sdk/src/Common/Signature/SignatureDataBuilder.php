<?php
namespace Bca\Api\Sdk\Common\Signature;

class SignatureDataBuilder
{
    protected $httpMethod;
    protected $rawRelativeUrl;
    protected $accessToken;
    protected $rawRequestBody;
    protected $timestamp;

    /**
     * @return string
     */
    public function getHttpMethod()
    {
        return $this->httpMethod;
    }

    /**
     * @return string
     */
    public function getRawRelativeUrl()
    {
        return $this->rawRelativeUrl;
    }

    /**
     * @return string
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @return string
     */
    public function getRawRequestBody()
    {
        return $this->rawRequestBody;
    }

    /**
     * @return string
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param string $httpMethod
     * @return SignatureDataBuilder
     */
    public function httpMethod($httpMethod)
    {
        $this->httpMethod = (string)$httpMethod;
        return $this;
    }

    /**
     * @param string $rawRelativeUrl
     * @return SignatureDataBuilder
     */
    public function rawRelativeUrl($rawRelativeUrl)
    {
        $this->rawRelativeUrl = (string)$rawRelativeUrl;
        return $this;
    }

    /**
     * @param string $accessToken
     * @return SignatureDataBuilder
     */
    public function accessToken($accessToken)
    {
        $this->accessToken = (string)$accessToken;
        return $this;
    }

    /**
     * @param string $rawRequestBody
     * @return SignatureDataBuilder
     */
    public function rawRequestBody($rawRequestBody)
    {
        $this->rawRequestBody = (string)$rawRequestBody;
        return $this;
    }

    /**
     * @param string $timestamp
     * @return SignatureDataBuilder
     */
    public function timestamp($timestamp)
    {
        $this->timestamp = (string)$timestamp;
        return $this;
    }

    /**
     * @return SignatureData
     */
    public function build()
    {
        return new SignatureData($this);
    }
}