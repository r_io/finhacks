<?php
namespace Bca\Api\Sdk\Common\Http;

use Bca\Api\Sdk\Common\Config\BaseApiConfig;
use Bca\Api\Sdk\Common\Exceptions\ApiRequestException;
use Bca\Api\Sdk\Common\Http\BcaApiHeaderBuilder;
use Bca\Api\Sdk\Common\Http\HttpClient;
use Bca\Api\Sdk\Common\Http\Models\ErrorBody;
use Bca\Api\Sdk\Common\Http\Models\HttpHeader;
use Bca\Api\Sdk\Common\OAuth2\OAuth2Client;
use Bca\Api\Sdk\Common\Signature\SignatureCalculator;
use Bca\Api\Sdk\Common\Signature\SignatureDataBuilder;
use Bca\Api\Sdk\Common\Utils\Formatter;
use Bca\Api\Sdk\Common\Utils\JsonParser;
use Bca\Api\Sdk\Common\Utils\Logger\LoggerFactory;

class RequestHelper
{
    use \Bca\Api\Sdk\Common\Utils\HasLogger;

    protected $config;
    protected $oAuth2Client;
    protected $httpClient;
    protected $jsonParser;

    public function __construct(BaseApiConfig $config, OAuth2Client $oAuth2Client, HttpClient $httpClient, JsonParser $jsonParser)
    {
        $this->config = $config;
        $this->oAuth2Client = $oAuth2Client;
        $this->httpClient = $httpClient;
        $this->jsonParser = $jsonParser;
    }

    /**
     * @param string $method HTTP method
     * @param string $url Full URL
     * @param string|object $body
     * @param string $successObjectClass Response class name
     * @param array $headers this HTTP headers will be added in request
     * @return $successObjectClass::class
     * @throws ApiRequestException, OAuth2Exception, HttpRequestException, JsonParsingException
     */
    public function fetchAndWrapResponse($method, $url, $body, $successObjectClass, $headers = array())
    {
        $response = $this->fetchWithoutWrap($method, $url, $body, $headers);
        if (preg_match('#^2\d\d$#', $response->getStatusCode())) {
            return $this->jsonParser->fromJson($response->getResponseBody(), $successObjectClass);
        } else {
            $errorBody = $this->jsonParser->fromJson($response->getResponseBody(), ErrorBody::getClass());
            throw new ApiRequestException($response->getStatusCode(), $errorBody);
        }
    }

    /**
     * @param string $method HTTP method
     * @param string $url Full URL
     * @param string|object $body
     * @param array $headers this HTTP headers will be added in request
     * @return \GuzzleHttp\Message\Response
     * @throws OAuth2Exception, HttpRequestException, JsonParsingException
     */
    public function fetchWithoutWrap($method, $url, $body, $headers = array())
    {
        if (!is_string($body)) {
            $body = $this->jsonParser->toJson($body);
        }

        static::getLogger()->info('Retrieve access token');
        $accessToken = $this->oAuth2Client->getAccessToken();

        return $this->fetchResponse($method, $url, $body, $accessToken, $headers);
    }

    protected function fetchResponse($method, $url, $body, $accessToken, $additionalHeaders = array())
    {
        static::getLogger()->info('Populate request header');
        $headers = $this->buildHeaders($method, $url, $body, $accessToken);
        if ($additionalHeaders && is_array($additionalHeaders)) {
            $headers = array_merge($headers, $this->arrayToHeaders($additionalHeaders));
        }
        return $this->httpClient->request($method, $url, $headers, $body);
    }

    protected function buildHeaders($method, $url, $body, $accessToken)
    {
        $time = Formatter::ISO8601Time(time());

        static::getLogger()->info('Generate signature');
        $signature = $this->buildSignature($method, $url, $body, $accessToken, $time);

        $headerBuilder = new BcaApiHeaderBuilder();
        $headerBuilder->authorization($accessToken)
            ->contentType('application/json')
            ->origin($this->config->getOrigin())
            ->apiKey($this->config->getApiKey())
            ->timestamp($time)
            ->signature($signature);
        return $headerBuilder->build();
    }

    protected function buildSignature($method, $url, $body, $accessToken, $time)
    {
        $signatureBuilder = new SignatureDataBuilder();
        $signatureBuilder->httpMethod($method)
            ->rawRelativeUrl($url)
            ->accessToken($accessToken)
            ->rawRequestBody($body)
            ->timestamp($time);
        $signData = $signatureBuilder->build();
        return SignatureCalculator::sign($signData, $this->config->getApiSecret());
    }

    protected function arrayToHeaders($array)
    {
        $result = array();
        foreach ($array as $key => $value) {
            $header = new HttpHeader();
            $header->setKey($key);
            $header->setValue($value);
            $result[] = $header;
        }
        return $result;
    }
}