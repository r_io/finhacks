<?php
namespace Bca\Api\Sdk\Common\Http;

interface HttpClient
{
    /**
     * @param string $url
     * @param HttpHeader[] $headers
     * @return HttpResponse
     * @throws HttpRequestException
     */
    public function get($url, $headers);

    /**
     * @param string $url
     * @param HttpHeader[] $headers
     * @param string $body
     * @return HttpResponse
     * @throws HttpRequestException
     */
    public function post($url, $headers, $body);

    /**
     * @param string $url
     * @param HttpHeader[] $headers
     * @param string $body
     * @return HttpResponse
     * @throws HttpRequestException
     */
    public function put($url, $headers, $body);

    /**
     * @param string $url
     * @param HttpHeader[] $headers
     * @return HttpResponse
     * @throws HttpRequestException
     */
    public function delete($url, $headers);

    /**
     * @param string $method
     * @param string $url
     * @param HttpHeader[] $headers
     * @param string $body
     * @return HttpResponse
     * @throws HttpRequestException
     */
    public function request($method, $url, $headers, $body);

}