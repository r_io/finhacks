<?php
namespace Bca\Api\Sdk\Common\Http\Models;

class HttpHeader
{
    use \Bca\Api\Sdk\Common\Utils\HasGetClass;

    protected $key;
    protected $value;

    public function __construct($key = null, $value = null)
    {
        if ($key !== null) {
            $this->setKey($key);
        }
        if ($value !== null) {
            $this->setValue($value);
        }
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = (string)$key;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = (string)$value;
    }
}