<?php
namespace Bca\Api\Sdk\Common\Http\Models;

use Bca\Api\Sdk\Common\Utils\JsonUnserializable;

class ErrorBody implements JsonUnserializable
{
    use \Bca\Api\Sdk\Common\Utils\HasGetClass;

    public static function fromJson($json)
    {
        $decoded = json_decode($json);
        $obj = new static();
        $obj->errorCode = $decoded->ErrorCode;
        $obj->errorMessage = ErrorMessage::fromJson(json_encode($decoded->ErrorMessage));
        return $obj;
    }

    protected $errorCode;
    protected $errorMessage;

    /**
     * @param string $errorCode
     * @param ErrorMessage $errorMessage
     */
    public function __construct($errorCode = null, ErrorMessage $errorMessage = null)
    {
        if (!empty($errorCode)) {
            $this->errorCode = $errorCode;
        }
        if (!empty($errorMessage)) {
            $this->errorMessage = $errorMessage;
        }
    }

    /**
     * @return string
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param string $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = (string)$errorCode;
    }

    /**
     * @return ErrorMessage
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @param ErrorMessage $errorMessage
     */
    public function setErrorMessage(ErrorMessage $errorMessage)
    {
        $this->errorMessage = $errorMessage;
    }

    public function __toString()
    {
        $encoded = [
            'ErrorCode' => $this->errorCode,
            'ErrorMessage' => [
                'Indonesian' => $this->errorMessage->getIndonesian(),
                'English' => $this->errorMessage->getEnglish(),
            ],
        ];
        return json_encode($encoded);
    }
}