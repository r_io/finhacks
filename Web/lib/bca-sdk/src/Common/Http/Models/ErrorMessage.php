<?php
namespace Bca\Api\Sdk\Common\Http\Models;

use Bca\Api\Sdk\Common\Utils\JsonUnserializable;

class ErrorMessage implements JsonUnserializable
{
    use \Bca\Api\Sdk\Common\Utils\HasGetClass;

    public static function fromJson($json)
    {
        $decoded = json_decode($json);
        $obj = new static();
        $obj->indonesian = $decoded->Indonesian;
        $obj->english = $decoded->English;
        return $obj;
    }

    protected $indonesian;
    protected $english;

    /**
     * @param string $indonesian
     * @param string $english
     */
    public function __construct($indonesian = null, $english = null)
    {
        if (!empty($indonesian)) {
            $this->indonesian = $indonesian;
        }
        if (!empty($english)) {
            $this->english = $english;
        }
    }

    /**
     * @return string
     */
    public function getIndonesian()
    {
        return $this->indonesian;
    }

    /**
     * @param string $indonesian
     */
    public function setIndonesian($indonesian)
    {
        $this->indonesian = (string)$indonesian;
    }

    /**
     * @return string
     */
    public function getEnglish()
    {
        return $this->english;
    }

    /**
     * @param string $english
     */
    public function setEnglish($english)
    {
        $this->english = (string)$english;
    }
}