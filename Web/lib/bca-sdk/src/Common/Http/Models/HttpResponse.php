<?php
namespace Bca\Api\Sdk\Common\Http\Models;

class HttpResponse
{
    use \Bca\Api\Sdk\Common\Utils\HasGetClass;

    protected $statusCode;
    protected $responseBody;
    protected $headers = [];

    /**
     * @return string
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param string $statusCode
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = (string)$statusCode;
    }

    /**
     * @return string
     */
    public function getResponseBody()
    {
        return $this->responseBody;
    }

    /**
     * @param string $responseBody
     */
    public function setResponseBody($responseBody)
    {
        $this->responseBody = (string)$responseBody;
    }

    /**
     * @return HttpHeader[]
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param HttpHeader[] $headers
     */
    public function setHeaders(array $headers)
    {
        $this->headers = $headers;
    }
}