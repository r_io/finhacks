<?php
namespace Bca\Api\Sdk\Common\Http;

class HeaderName
{
    const AUTHORIZATION = 'Authorization';
    const CONTENT_TYPE = 'Content-Type';
    const ORIGIN = 'Origin';
    const BCA_KEY = 'X-BCA-Key';
    const BCA_TIMESTAMP = 'X-BCA-Timestamp';
    const BCA_SIGNATURE = 'X-BCA-Signature';
}