<?php
namespace Bca\Api\Sdk\Common\Http;

use Bca\Api\Sdk\Common\Http\Models\HttpHeader;

class BcaApiHeaderBuilder
{
    protected $authorization;
    protected $contentType;
    protected $origin;
    protected $apiKey;
    protected $timestamp;
    protected $signature;

    protected function createHeader($key, $value)
    {
        $header = new HttpHeader();
        $header->setKey($key);
        $header->setValue($value);
        return $header;
    }

    /**
     * @param string $accessToken
     * @return BcaApiHeaderBuilder
     */
    public function authorization($accessToken)
    {
        $this->authorization = $this->createHeader(HeaderName::AUTHORIZATION, 'Bearer '.$accessToken);
        return $this;
    }

    /**
     * @param string $contentType
     * @return BcaApiHeaderBuilder
     */
    public function contentType($contentType)
    {
        $this->contentType = $this->createHeader(HeaderName::CONTENT_TYPE, $contentType);
        return $this;
    }

    /**
     * @param string $origin
     * @return BcaApiHeaderBuilder
     */
    public function origin($origin)
    {
        $this->origin = $this->createHeader(HeaderName::ORIGIN, $origin);
        return $this;
    }

    /**
     * @param string $apiKey
     * @return BcaApiHeaderBuilder
     */
    public function apiKey($apiKey)
    {
        $this->apiKey = $this->createHeader(HeaderName::BCA_KEY, $apiKey);
        return $this;
    }

    /**
     * @param string $timestampISO8601
     * @return BcaApiHeaderBuilder
     */
    public function timestamp($timestampISO8601)
    {
        $this->timestamp = $this->createHeader(HeaderName::BCA_TIMESTAMP, $timestampISO8601);
        return $this;
    }

    /**
     * @param string $signature
     * @return BcaApiHeaderBuilder
     */
    public function signature($signature)
    {
        $this->signature = $this->createHeader(HeaderName::BCA_SIGNATURE, $signature);
        return $this;
    }

    /**
     * @return HttpHeader[]
     */
    public function build()
    {
        return [
            $this->authorization, $this->contentType,
            $this->origin, $this->apiKey,
            $this->timestamp, $this->signature,
        ];
    }
}