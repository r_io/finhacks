<?php
namespace Bca\Api\Sdk\Common\Http;
/**
 * @uses Guzzle
 */
use Bca\Api\Sdk\Common\Exceptions\HttpRequestException;
use Bca\Api\Sdk\Common\Http\Models\HttpHeader;
use Bca\Api\Sdk\Common\Http\Models\HttpResponse;
use Bca\Api\Sdk\Common\Utils\Logger\LoggerFactory;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Message\Request;

class DefaultHttpClient implements HttpClient
{
    use \Bca\Api\Sdk\Common\Utils\HasLogger;

    // because CURL_SSLVERSION_TLSv1_2 doesn't exist on PHP 5.4.0
    const TLS_v1_2 = 6;

    /**
     * @param string $url
     * @param HttpHeader[] $headers
     * @return HttpResponse
     * @throws HttpRequestException
     */
    public function get($url, $headers)
    {
        return $this->request('GET', $url, $headers, null);
    }

    /**
     * @param string $url
     * @param HttpHeader[] $headers
     * @param string $body
     * @return HttpResponse
     * @throws HttpRequestException
     */
    public function post($url, $headers, $body)
    {
        return $this->request('POST', $url, $headers, $body);
    }

    /**
     * @param string $url
     * @param HttpHeader[] $headers
     * @param string $body
     * @return HttpResponse
     * @throws HttpRequestException
     */
    public function put($url, $headers, $body)
    {
        return $this->request('PUT', $url, $headers, $body);
    }

    /**
     * @param string $url
     * @param HttpHeader[] $headers
     * @return HttpResponse
     * @throws HttpRequestException
     */
    public function delete($url, $headers)
    {
        return $this->request('DELETE', $url, $headers, null);
    }

    /**
     * @param string $method
     * @param string $url
     * @param HttpHeader[] $headers
     * @param string $body
     * @return HttpResponse
     * @throws HttpRequestException
     */
    public function request($method, $url, $headers, $body)
    {
        $arrayHeaders = $this->httpHeadersToArrayHeaders($headers);
        $guzzleClient = new Client(['http_errors' => false, 'verify' => true]);
        $guzzleRequest = $guzzleClient->createRequest($method, $url, array(
            'body' => $body,
            'headers' => $arrayHeaders,
            'config' => [
                'curl' => $this->getCurlConfig(),
            ],
        ));

        static::getLogger()->info("Request API {$method} {$url}");
        static::getLogger()->debug("Headers: ".json_encode($arrayHeaders));
        static::getLogger()->debug("Request body = $body");

        try {
            $guzzleResponse = $guzzleClient->send($guzzleRequest);
        } catch (BadResponseException $exc) {
            $guzzleResponse = $exc->getResponse();
        } catch (\Exception $exc) {
            throw new HttpRequestException("Guzzle Client throw exception: {$exc->getMessage()}", $exc->getCode(), $exc);
        }

        $httpResponse = new HttpResponse();
        $httpResponse->setStatusCode($guzzleResponse->getStatusCode());
        $httpResponse->setResponseBody($guzzleResponse->getBody());
        $httpResponse->setHeaders($this->guzzleHeadersToHttpHeaders($guzzleResponse->getHeaders()));

        static::getLogger()->info("Request return with {$httpResponse->getStatusCode()}");
        static::getLogger()->debug("Response body = {$httpResponse->getResponseBody()}");

        return $httpResponse;
    }

    protected function httpHeadersToArrayHeaders($httpHeaders)
    {
        $arrayHeaders = [];
        foreach ($httpHeaders as $header) {
            $arrayHeaders[$header->getKey()] = $header->getValue();
        }
        return $arrayHeaders;
    }

    protected function getCurlConfig()
    {
        return [
            CURLOPT_SSLVERSION => self::TLS_v1_2,
        ];
    }

    protected function guzzleHeadersToHttpHeaders($guzzleHeaders)
    {
        $httpHeaders = [];
        foreach ($guzzleHeaders as $name => $values) {
            $header = new HttpHeader();
            $header->setKey($name);
            $header->setValue(implode(', ', $values));
            $httpHeaders[] = $header;
        }
        return $httpHeaders;
    }
}