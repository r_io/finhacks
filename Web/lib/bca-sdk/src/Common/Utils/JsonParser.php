<?php
namespace Bca\Api\Sdk\Common\Utils;

interface JsonParser
{
    /**
     * @param mixed $source
     * @return string
     */
    public function toJson($source);

    /**
     * @param string $json
     * @param string $class
     * @return Object specified by $class
     */
    public function fromJson($json, $class);
}