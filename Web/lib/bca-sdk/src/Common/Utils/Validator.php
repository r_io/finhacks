<?php
namespace Bca\Api\Sdk\Common\Utils;

class Validator
{
    /**
     * @param string $value
     * @throws InvalidArgumentException
     */
    public static function shouldNotNullOrEmpty($value)
    {
        if (is_null($value) || (strlen(trim($value)) == 0)) {
            throw new \InvalidArgumentException('Value should not be null or empty');
        }
    }

    /**
     * @param string $value
     * @throws InvalidArgumentException
     */
    public static function shouldBePhoneNumber($value)
    {
        if (!preg_match('#^(\+|0)\d+$#', $value)) {
            throw new \InvalidArgumentException('Phone number should start with "+62" or "0"');
        }
    }
}