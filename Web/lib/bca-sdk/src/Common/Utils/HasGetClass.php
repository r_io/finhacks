<?php
namespace Bca\Api\Sdk\Common\Utils;

trait HasGetClass
{
    public static function getClass()
    {
        return get_called_class();
    }
}