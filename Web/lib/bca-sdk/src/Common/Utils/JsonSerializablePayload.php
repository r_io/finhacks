<?php
namespace Bca\Api\Sdk\Common\Utils;

class JsonSerializablePayload implements \JsonSerializable
{
    use \Bca\Api\Sdk\Common\Utils\HasGetClass;

    public function jsonSerialize()
    {
        $properties = get_object_vars($this);
        $result = [];
        foreach ($properties as $propertyName => $value) {
            if (!is_null($value)) {
                $result[ucfirst($propertyName)] = $value;
            }
        }
        return $result;
    }
}