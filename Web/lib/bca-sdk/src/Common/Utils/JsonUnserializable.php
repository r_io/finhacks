<?php
namespace Bca\Api\Sdk\Common\Utils;

interface JsonUnserializable
{
    /**
     * @return <Class>
     */
    public static function fromJson($json);
}