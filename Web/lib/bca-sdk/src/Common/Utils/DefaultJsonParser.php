<?php
namespace Bca\Api\Sdk\Common\Utils;

use Bca\Api\Sdk\Common\Exceptions\JsonParsingException;
use Bca\Api\Sdk\Common\Utils\JsonUnserializable;

class DefaultJsonParser implements JsonParser
{
    /**
     * @param mixed $source
     * @return string
     * @throws JsonParsingException
     */
    public function toJson($source)
    {
        $json = json_encode($source, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        if (json_last_error() === JSON_ERROR_NONE) {
            return $json;
        } else {
            throw new JsonParsingException(json_last_error_msg());
        }
    }

    /**
     * @param string $json
     * @param string $class
     * @return Object specified by $class
     * @throws JsonParsingException
     */
    public function fromJson($json, $class)
    {
        $implements = class_implements($class);
        if (isset($implements['Bca\Api\Sdk\Common\Utils\JsonUnserializable'])) {
            return call_user_func([$class, 'fromJson'], $json);
        } else {
            throw new JsonParsingException('Unsupported class, must implement JsonUnserializable');
        }
    }
}