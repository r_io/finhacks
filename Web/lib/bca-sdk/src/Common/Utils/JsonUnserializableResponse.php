<?php
namespace Bca\Api\Sdk\Common\Utils;

class JsonUnserializableResponse implements JsonUnserializable
{
    use \Bca\Api\Sdk\Common\Utils\HasGetClass;

    public static function fromJson($json)
    {
        $properties = get_class_vars(get_called_class());
        $decoded = json_decode($json);
        $obj = new static();

        foreach ($properties as $propertyName => $defaultValue) {
            $jsonKey = ucfirst($propertyName);
            if (isset($decoded->$jsonKey)) $obj->$propertyName = $decoded->$jsonKey;
        }
        return $obj;
    }
}