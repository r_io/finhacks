<?php
namespace Bca\Api\Sdk\Common\Utils;

class Formatter
{
    const REGEX_VALID_TIME = '/\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/';

    /**
     * @param mixed $inputTime
     * @return string ISO8601-compatible timestamp
     */
    public static function ISO8601Time($inputTime)
    {
        if (is_numeric($inputTime)) {
            $timestamp = $inputTime;
        } else if (is_string($inputTime)) {
            if (preg_match(static::REGEX_VALID_TIME, $inputTime)) {
                return $inputTime;
            }
            $timestamp = strtotime($inputTime);
        } else if ($inputTime instanceof DateTime) {
            $timestamp = $inputTime->getTimestamp();
        }
        return date('Y-m-d\TH:i:s.000P', $timestamp);
    }

    /**
     * @param string|int $amount
     * @return string Number format 13.2
     */
    public static function amount($amount)
    {
        return sprintf('%0.02f', $amount);
    }

    /**
     * @param array $data key-value
     * @return string as HTTP query
     */
    public static function httpQuery(array $data)
    {
        return http_build_query($data, null, '&', PHP_QUERY_RFC3986);
    }
}