<?php
namespace Bca\Api\Sdk\Common\Utils\Logger;

interface LoggerDriver
{
    /**
     * Logs with an arbitrary level.
     *
     * @param string  $level
     * @param string $message
     * @param array  $context
     */
    public function log($level, $message, array $context = array());
}