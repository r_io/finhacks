<?php
namespace Bca\Api\Sdk\Common\Utils\Logger;

use Psr\Log\AbstractLogger;

class LoggerImplementation extends AbstractLogger
{
    protected $driver;
    protected $currentClass = '';

    public function __construct(LoggerDriver $driver)
    {
        $this->driver = $driver;
    }

    public function setCurrentClass($class)
    {
        $this->currentClass = $class;
    }

    public function log($level, $message, array $context = array())
    {
        $this->driver->log($level, "({$this->currentClass}) ".$message, $context);
    }
}