<?php
namespace Bca\Api\Sdk\Common\Utils\Logger;

class LoggerFactory
{
    protected static $instance = null;

    /**
     * @param string $class Caller class name
     * @return LoggerInterface PSR-3 Logger Interface
     */
    public static function getLogger($class)
    {
        static::getInstance()->setCurrentClass($class);
        return static::getInstance();
    }

    public static function setDriver(LoggerDriver $driver)
    {
        static::$instance = new LoggerImplementation($driver);
    }

    protected static function getInstance()
    {
        if (static::$instance === null) {
            static::$instance = new LoggerImplementation(new NoOpLoggerDriver());
        }
        return static::$instance;
    }
}