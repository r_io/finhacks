<?php
namespace Bca\Api\Sdk\Common\Utils\Logger;

class NoOpLoggerDriver implements LoggerDriver
{
    public function log($level, $message, array $context = array())
    {

    }
}