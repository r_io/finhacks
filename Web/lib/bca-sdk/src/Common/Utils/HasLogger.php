<?php
namespace Bca\Api\Sdk\Common\Utils;

use Bca\Api\Sdk\Common\Utils\Logger\LoggerFactory;

trait HasLogger
{
    public static function getLogger()
    {
        return LoggerFactory::getLogger(get_called_class());
    }
}