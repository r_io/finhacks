<?php
namespace Bca\Api\Sdk\Common\Exceptions;

class OAuth2Exception extends \RuntimeException
{
    const INVALID_REQUEST = 1;
    const INVALID_CLIENT = 2;
    const INVALID_GRANT = 3;
    const UNAUTHORIZED_CLIENT = 4;
    const UNSUPPORTED_GRANT_TYPE = 5;
    const INVALID_SCOPE = 6;
}