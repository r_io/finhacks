<?php
namespace Bca\Api\Sdk\Common\Exceptions;

use Bca\Api\Sdk\Common\Http\Models\ErrorBody;

class ApiRequestException extends \RuntimeException
{
    protected $statusCode;
    protected $body;

    public function __construct($statusCode, ErrorBody $body, \Throwable $cause = null)
    {
        $this->statusCode = (string)$statusCode;
        $this->body = $body;

        parent::__construct("Server returns status code: {$statusCode} with body: {$body}", 1, $cause);
    }

    /**
     * @return string
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @return ErrorBody
     */
    public function getBody()
    {
        return $this->body;
    }
}