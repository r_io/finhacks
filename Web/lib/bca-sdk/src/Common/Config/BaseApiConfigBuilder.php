<?php
namespace Bca\Api\Sdk\Common\Config;

use Bca\Api\Sdk\Common\Utils\Validator;

class BaseApiConfigBuilder
{
    protected $apiKey;
    protected $apiSecret;
    protected $clientId;
    protected $clientSecret;
    protected $origin;
    protected $baseApiUri;
    protected $baseOAuth2Uri;
    protected $scopes = [];

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @return string
     */
    public function getApiSecret()
    {
        return $this->apiSecret;
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @return string
     */
    public function getClientSecret()
    {
        return $this->clientSecret;
    }

    /**
     * @return string
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * @return string
     */
    public function getBaseApiUri()
    {
        return $this->baseApiUri;
    }

    /**
     * @return string
     */
    public function getBaseOAuth2Uri()
    {
        return $this->baseOAuth2Uri;
    }

    /**
     * @return string[]
     */
    public function getScopes()
    {
        return $this->scopes;
    }

    /**
     * @param string $apiKey
     * @return BaseApiConfigBuilder
     */
    public function apiKey($apiKey)
    {
        $this->apiKey = (string)$apiKey;
        return $this;
    }

    /**
     * @param string $apiSecret
     * @return BaseApiConfigBuilder
     */
    public function apiSecret($apiSecret)
    {
        $this->apiSecret = (string)$apiSecret;
        return $this;
    }

    /**
     * @param string $clientId
     * @return BaseApiConfigBuilder
     */
    public function clientId($clientId)
    {
        $this->clientId = (string)$clientId;
        return $this;
    }

    /**
     * @param string $clientSecret
     * @return BaseApiConfigBuilder
     */
    public function clientSecret($clientSecret)
    {
        $this->clientSecret = (string)$clientSecret;
        return $this;
    }

    /**
     * @param string $origin
     * @return BaseApiConfigBuilder
     */
    public function origin($origin)
    {
        $this->origin = (string)$origin;
        return $this;
    }

    /**
     * @param string $baseApiUri
     * @return BaseApiConfigBuilder
     */
    public function baseApiUri($baseApiUri)
    {
        $this->baseApiUri = (string)$baseApiUri;
        return $this;
    }

    /**
     * @param string $baseOAuth2Uri
     * @return BaseApiConfigBuilder
     */
    public function baseOAuth2Uri($baseOAuth2Uri)
    {
        $this->baseOAuth2Uri = (string)$baseOAuth2Uri;
        return $this;
    }

    /**
     * @param string[] $scopes
     * @return BaseApiConfigBuilder
     */
    public function scopes(array $scopes)
    {
        $this->scopes = $scopes;
        return $this;
    }

    // Child classes must implement their own `build()` method

    protected function validateProperties()
    {
        Validator::shouldNotNullOrEmpty($this->apiKey);
        Validator::shouldNotNullOrEmpty($this->apiSecret);
        Validator::shouldNotNullOrEmpty($this->clientId);
        Validator::shouldNotNullOrEmpty($this->clientSecret);
        Validator::shouldNotNullOrEmpty($this->origin);
        Validator::shouldNotNullOrEmpty($this->baseApiUri);
        Validator::shouldNotNullOrEmpty($this->baseOAuth2Uri);
        // Validator::shouldNotNullOrEmpty($this->scopes);
    }
}