<?php
namespace Bca\Api\Sdk\Common\Config;

use Bca\Api\Sdk\Common\Config\BaseApiConfigBuilder;

class BaseApiConfig
{
    use \Bca\Api\Sdk\Common\Utils\HasGetClass;

    const ACCESS_TOKEN_PATH = '/api/oauth/token';

    protected $apiKey;
    protected $apiSecret;
    protected $clientId;
    protected $clientSecret;
    protected $origin;
    protected $baseApiUri;
    protected $baseOAuth2Uri;
    protected $scopes;
    protected $accessTokenUri;

    public function __construct(BaseApiConfigBuilder $builder)
    {
        $this->apiKey = $builder->getApiKey();
        $this->apiSecret = $builder->getApiSecret();
        $this->clientId = $builder->getClientId();
        $this->clientSecret = $builder->getClientSecret();
        $this->origin = $builder->getOrigin();
        $this->baseApiUri = rtrim($builder->getBaseApiUri(), '/');
        $this->baseOAuth2Uri = rtrim($builder->getBaseOAuth2Uri(), '/');
        $this->scopes = $builder->getScopes();
        $this->accessTokenUri = $this->baseOAuth2Uri . static::ACCESS_TOKEN_PATH;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @return string
     */
    public function getApiSecret()
    {
        return $this->apiSecret;
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @return string
     */
    public function getClientSecret()
    {
        return $this->clientSecret;
    }

    /**
     * @return string
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * @return string
     */
    public function getBaseApiUri()
    {
        return $this->baseApiUri;
    }

    /**
     * @return string
     */
    public function getBaseOAuth2Uri()
    {
        return $this->baseOAuth2Uri;
    }

    /**
     * @return string[]
     */
    public function getScopes()
    {
        return $this->scopes;
    }

    /**
     * @return string
     */
    public function getAccessTokenUri()
    {
        return $this->accessTokenUri;
    }

    public function __toString()
    {
        return print_r(get_object_vars($this), true);
    }
}