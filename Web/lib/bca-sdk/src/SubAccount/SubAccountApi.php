<?php
namespace Bca\Api\Sdk\SubAccount;

use Bca\Api\Sdk\Common\Http\DefaultHttpClient;
use Bca\Api\Sdk\Common\Http\HttpClient;
use Bca\Api\Sdk\Common\Http\RequestHelper;
use Bca\Api\Sdk\Common\OAuth2\Grant\Client\ClientCredentialsOAuth2Client;
use Bca\Api\Sdk\Common\OAuth2\Grant\Client\ClientCredentialsOAuth2Details;
use Bca\Api\Sdk\Common\Utils\DefaultJsonParser;
use Bca\Api\Sdk\Common\Utils\Logger\LoggerFactory;
use Bca\Api\Sdk\SubAccount\Models\Requests\OtpGenerationPayload;
use Bca\Api\Sdk\SubAccount\Models\Requests\TopUpInquiryParams;
use Bca\Api\Sdk\SubAccount\Models\Requests\TopUpPayload;
use Bca\Api\Sdk\SubAccount\Models\Requests\TransactionInquiryParams;
use Bca\Api\Sdk\SubAccount\Models\Requests\TransferCompanyInquiryParams;
use Bca\Api\Sdk\SubAccount\Models\Requests\TransferCompanyPayload;
use Bca\Api\Sdk\SubAccount\Models\Requests\UserRegistrationPayload;
use Bca\Api\Sdk\SubAccount\Models\Requests\UserUpdatePayload;
use Bca\Api\Sdk\SubAccount\Models\Responses\OtpGenerationResponse;
use Bca\Api\Sdk\SubAccount\Models\Responses\TopUpResponse;
use Bca\Api\Sdk\SubAccount\Models\Responses\TransactionInquiryResponse;
use Bca\Api\Sdk\SubAccount\Models\Responses\TransferCompanyResponse;
use Bca\Api\Sdk\SubAccount\Models\Responses\UserInquiryResponse;
use Bca\Api\Sdk\SubAccount\Models\Responses\UserRegistrationResponse;
use Bca\Api\Sdk\SubAccount\Models\Responses\UserUpdateResponse;
use Bca\Api\Sdk\SubAccount\SubAccountApiConfig;

class SubAccountApi
{
    use \Bca\Api\Sdk\Common\Utils\HasGetClass;
    use \Bca\Api\Sdk\Common\Utils\HasLogger;

    protected $config;
    protected $helper;

    public function __construct(SubAccountApiConfig $config)
    {
        static::getLogger()->info('Configure SubAccount Api');
        static::getLogger()->debug('SubAccountApi configs: ' . $config);

        $this->config = $config;

        $httpClient = new DefaultHttpClient();
        $details = new ClientCredentialsOAuth2Details($config);
        $oAuth2Client = new ClientCredentialsOAuth2Client($details, $httpClient);

        $this->helper = new RequestHelper($config, $oAuth2Client, $httpClient, new DefaultJsonParser());
    }

    public function setHttpClient(HttpClient $httpClient)
    {
        $details = new ClientCredentialsOAuth2Details($this->config);
        $oAuth2Client = new ClientCredentialsOAuth2Client($details, $httpClient);

        $this->helper = new RequestHelper($this->config, $oAuth2Client, $httpClient, new DefaultJsonParser());
    }

    public function registerUser(UserRegistrationPayload $payload)
    {
        static::getLogger()->info('Handle registerUser');

        $payload->setCompanyCode($this->config->getCompanyCode());
        $url = $this->config->getUserRegistrationUri();
        return $this->helper->fetchAndWrapResponse(
            'POST',
            $url,
            $payload,
            UserRegistrationResponse::getClass()
        );
    }

    public function inquiryUser($primaryID)
    {
        static::getLogger()->info('Handle inquiryUser');

        $url = sprintf($this->config->getUserInquiryUri(), $this->config->getCompanyCode(), $primaryID);
        return $this->helper->fetchAndWrapResponse(
            'GET',
            $url,
            '',
            UserInquiryResponse::getClass()
        );
    }

    public function updateUser($primaryID, UserUpdatePayload $payload)
    {
        static::getLogger()->info('Handle updateUser');

        $url = sprintf($this->config->getUserUpdateUri(), $this->config->getCompanyCode(), $primaryID);
        return $this->helper->fetchAndWrapResponse(
            'PUT',
            $url,
            $payload,
            UserUpdateResponse::getClass()
        );
    }

    public function topUp(TopUpPayload $payload)
    {
        static::getLogger()->info('Handle topUp');

        $payload->setCompanyCode($this->config->getCompanyCode());
        $url = $this->config->getTopUpUri();
        return $this->helper->fetchAndWrapResponse(
            'POST',
            $url,
            $payload,
            TopUpResponse::getClass()
        );
    }

    public function inquiryTopUp($primaryID, TopUpInquiryParams $params)
    {
        static::getLogger()->info('Handle inquiryTopUp');

        $url = sprintf($this->config->getTopUpInquiryUri(), $this->config->getCompanyCode(), $primaryID) .
            '?' . (string)$params;
        return $this->helper->fetchAndWrapResponse(
            'GET',
            $url,
            '',
            TopUpResponse::getClass()
        );
    }

    public function transactionInquiry($primaryID, TransactionInquiryParams $params)
    {
        static::getLogger()->info('Handle transactionInquiry');

        $url = sprintf($this->config->getTransactionInquiryUri(), $this->config->getCompanyCode(), $primaryID) .
            '?' . (string)$params;
        return $this->helper->fetchAndWrapResponse(
            'GET',
            $url,
            '',
            TransactionInquiryResponse::getClass()
        );
    }

    public function generateOtp(OtpGenerationPayload $payload)
    {
        static::getLogger()->info('Handle generateOtp');

        $payload->setCompanyCode($this->config->getCompanyCode());
        $url = $this->config->getOtpGenerationUri();
        return $this->helper->fetchAndWrapResponse(
            'POST',
            $url,
            $payload,
            OtpGenerationResponse::getClass()
        );
    }

    public function transferCompanyAccount(TransferCompanyPayload $payload)
    {
        static::getLogger()->info('Handle transferCompanyAccount');

        $payload->setCompanyCode($this->config->getCompanyCode());
        $url = $this->config->getTransferCompanyUri();
        return $this->helper->fetchAndWrapResponse(
            'POST',
            $url,
            $payload,
            TransferCompanyResponse::getClass()
        );
    }

    public function transferCompanyInquiry($primaryID, TransferCompanyInquiryParams $params)
    {
        static::getLogger()->info('Handle transferCompanyInquiry');

        $url = sprintf($this->config->getTransferCompanyInquiryUri(), $this->config->getCompanyCode(), $primaryID) .
            '?' . (string)$params;
        return $this->helper->fetchAndWrapResponse(
            'GET',
            $url,
            '',
            TransferCompanyResponse::getClass()
        );
    }
}