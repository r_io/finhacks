<?php
namespace Bca\Api\Sdk\SubAccount;

use Bca\Api\Sdk\Common\Config\BaseApiConfigBuilder;
use Bca\Api\Sdk\Common\Utils\Validator;

class SubAccountApiConfigBuilder extends BaseApiConfigBuilder
{
    protected $companyCode;

    public function getCompanyCode()
    {
        return $this->companyCode;
    }

    public function companyCode($companyCode)
    {
        $this->companyCode = (string)$companyCode;
        return $this;
    }

    public function build()
    {
        $this->validateProperties();
        return new SubAccountApiConfig($this);
    }

    protected function validateProperties()
    {
        parent::validateProperties();
        Validator::shouldNotNullOrEmpty($this->companyCode);
    }
}