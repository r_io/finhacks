<?php
namespace Bca\Api\Sdk\SubAccount;

use Bca\Api\Sdk\Common\Config\BaseApiConfig;

class SubAccountApiConfig extends BaseApiConfig
{
    const USER_REGISTRATION_PATH = '/ewallet/customers';
    const USER_INQUIRY_PATH = '/ewallet/customers/%s/%s';
    const USER_UPDATE_PATH = '/ewallet/customers/%s/%s';
    const TRANSFER_COMPANY_PATH = '/ewallet/transfers';
    const TRANSFER_COMPANY_INQUIRY_PATH = '/ewallet/transfers/%s/%s';
    const TOP_UP_PATH = '/ewallet/topup';
    const TOP_UP_INQUIRY_PATH = '/ewallet/topup/%s/%s';
    const TRANSACTION_INQUIRY_PATH = '/ewallet/transactions/%s/%s';
    const OTP_GENERATION_PATH = '/ewallet/otp/generate/cashout';

    protected $companyCode;
    protected $userRegistrationUri;
    protected $userInquiryUri;
    protected $userUpdateUri;
    protected $transferCompanyUri;
    protected $transferCompanyInquiryUri;
    protected $topUpUri;
    protected $topUpInquiryUri;
    protected $transactionInquiryUri;
    protected $otpGenerationUri;

    public function __construct(SubAccountApiConfigBuilder $builder)
    {
        parent::__construct($builder);
        $this->companyCode = $builder->getCompanyCode();
        $this->userRegistrationUri = $this->baseApiUri . static::USER_REGISTRATION_PATH;
        $this->userInquiryUri = $this->baseApiUri . static::USER_INQUIRY_PATH;
        $this->userUpdateUri = $this->baseApiUri . static::USER_UPDATE_PATH;
        $this->transferCompanyUri = $this->baseApiUri . static::TRANSFER_COMPANY_PATH;
        $this->transferCompanyInquiryUri = $this->baseApiUri . static::TRANSFER_COMPANY_INQUIRY_PATH;
        $this->topUpUri = $this->baseApiUri . static::TOP_UP_PATH;
        $this->topUpInquiryUri = $this->baseApiUri . static::TOP_UP_INQUIRY_PATH;
        $this->transactionInquiryUri = $this->baseApiUri . static::TRANSACTION_INQUIRY_PATH;
        $this->otpGenerationUri = $this->baseApiUri . static::OTP_GENERATION_PATH;
    }

    public function getCompanyCode()
    {
        return $this->companyCode;
    }

    public function getUserRegistrationUri()
    {
        return $this->userRegistrationUri;
    }

    public function getUserInquiryUri()
    {
        return $this->userInquiryUri;
    }

    public function getUserUpdateUri()
    {
        return $this->userUpdateUri;
    }

    public function getTransferCompanyUri()
    {
        return $this->transferCompanyUri;
    }

    public function getTransferCompanyInquiryUri()
    {
        return $this->transferCompanyInquiryUri;
    }

    public function getTopUpUri()
    {
        return $this->topUpUri;
    }

    public function getTopUpInquiryUri()
    {
        return $this->topUpInquiryUri;
    }

    public function getTransactionInquiryUri()
    {
        return $this->transactionInquiryUri;
    }

    public function getOtpGenerationUri()
    {
        return $this->otpGenerationUri;
    }
}