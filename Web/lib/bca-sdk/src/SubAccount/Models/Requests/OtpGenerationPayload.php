<?php
namespace Bca\Api\Sdk\SubAccount\Models\Requests;

use Bca\Api\Sdk\Common\Utils\JsonSerializablePayload;

class OtpGenerationPayload extends JsonSerializablePayload
{
    protected $companyCode;
    protected $customerNumber;
    protected $amount;

    public function getCompanyCode()
    {
        return $this->companyCode;
    }

    public function setCompanyCode($companyCode)
    {
        $this->companyCode = (string)$companyCode;
    }

    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }

    public function setCustomerNumber($customerNumber)
    {
        $this->customerNumber = (string)$customerNumber;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setAmount($amount)
    {
        $this->amount = (string)$amount;
    }
}