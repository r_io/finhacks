<?php
namespace Bca\Api\Sdk\SubAccount\Models\Requests;

use Bca\Api\Sdk\Common\Utils\Formatter;

class TransferCompanyInquiryParams
{
    const TRANSACTION_ID = 'TransactionID';
    const REQUEST_DATE = 'RequestDate';

    protected $transactionID;
    protected $requestDate;

    public function __toString()
    {
        return Formatter::httpQuery([
            static::TRANSACTION_ID => $this->transactionID,
            static::REQUEST_DATE => $this->requestDate,
        ]);
    }

    public function getTransactionID()
    {
        return $this->transactionID;
    }

    public function setTransactionID($transactionID)
    {
        $this->transactionID = (string)$transactionID;
    }

    public function getRequestDate()
    {
        return $this->requestDate;
    }

    public function setRequestDate($requestDate)
    {
        $this->requestDate = (string)$requestDate;
    }
}