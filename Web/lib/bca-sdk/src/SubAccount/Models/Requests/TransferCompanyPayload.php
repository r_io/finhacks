<?php
namespace Bca\Api\Sdk\SubAccount\Models\Requests;

use Bca\Api\Sdk\Common\Utils\JsonSerializablePayload;

class TransferCompanyPayload extends JsonSerializablePayload
{
    protected $companyCode;
    protected $primaryID;
    protected $transactionID;
    protected $requestDate;
    protected $amount;
    protected $currencyCode;

    public function getCompanyCode()
    {
        return $this->companyCode;
    }

    public function setCompanyCode($companyCode)
    {
        $this->companyCode = (string)$companyCode;
    }

    public function getPrimaryID()
    {
        return $this->primaryID;
    }

    public function setPrimaryID($primaryID)
    {
        $this->primaryID = (string)$primaryID;
    }

    public function getTransactionID()
    {
        return $this->transactionID;
    }

    public function setTransactionID($transactionID)
    {
        $this->transactionID = (string)$transactionID;
    }

    public function getRequestDate()
    {
        return $this->requestDate;
    }

    public function setRequestDate($requestDate)
    {
        $this->requestDate = (string)$requestDate;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setAmount($amount)
    {
        $this->amount = (string)$amount;
    }

    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = (string)$currencyCode;
    }
}