<?php
namespace Bca\Api\Sdk\SubAccount\Models\Requests;

use Bca\Api\Sdk\Common\Utils\Formatter;

class TransactionInquiryParams
{
    const START_DATE = 'StartDate';
    const END_DATE = 'EndDate';
    const LAST_ACCOUNT_STATEMENT_ID = 'LastAccountStatementID';

    protected $startDate;
    protected $endDate;
    protected $lastAccountStatementID;

    public function __toString()
    {
        return Formatter::httpQuery([
            static::START_DATE => $this->startDate,
            static::END_DATE => $this->endDate,
            static::LAST_ACCOUNT_STATEMENT_ID => $this->lastAccountStatementID,
        ]);
    }

    public function getStartDate()
    {
        return $this->startDate;
    }

    public function setStartDate($startDate)
    {
        $this->startDate = (string)$startDate;
    }

    public function getEndDate()
    {
        return $this->endDate;
    }

    public function setEndDate($endDate)
    {
        $this->endDate = (string)$endDate;
    }

    public function getLastAccountStatementID()
    {
        return $this->lastAccountStatementID;
    }

    public function setLastAccountStatementID($lastAccountStatementID)
    {
        $this->lastAccountStatementID = (string)$lastAccountStatementID;
    }
}