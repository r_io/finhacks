<?php
namespace Bca\Api\Sdk\SubAccount\Models\Requests;

use Bca\Api\Sdk\Common\Utils\JsonSerializablePayload;

class UserRegistrationPayload extends JsonSerializablePayload
{
    protected $customerName;
    protected $dateOfBirth;
    protected $primaryID;
    protected $mobileNumber;
    protected $emailAddress;
    protected $companyCode;
    protected $customerNumber;
    protected $IDNumber;

    public function getCustomerName()
    {
        return $this->customerName;
    }

    public function setCustomerName($customerName)
    {
        $this->customerName = (string)$customerName;
    }

    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = (string)$dateOfBirth;
    }

    public function getPrimaryID()
    {
        return $this->primaryID;
    }

    public function setPrimaryID($primaryID)
    {
        $this->primaryID = (string)$primaryID;
    }

    public function getMobileNumber()
    {
        return $this->mobileNumber;
    }

    public function setMobileNumber($mobileNumber)
    {
        $this->mobileNumber = (string)$mobileNumber;
    }

    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = (string)$emailAddress;
    }

    public function getCompanyCode()
    {
        return $this->companyCode;
    }

    public function setCompanyCode($companyCode)
    {
        $this->companyCode = (string)$companyCode;
    }

    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }

    public function setCustomerNumber($customerNumber)
    {
        $this->customerNumber = (string)$customerNumber;
    }

    public function getIDNumber()
    {
        return $this->IDNumber;
    }

    public function setIDNumber($IDNumber)
    {
        $this->IDNumber = (string)$IDNumber;
    }
}