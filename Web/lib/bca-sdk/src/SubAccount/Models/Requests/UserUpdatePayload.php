<?php
namespace Bca\Api\Sdk\SubAccount\Models\Requests;

use Bca\Api\Sdk\Common\Utils\JsonSerializablePayload;

class UserUpdatePayload extends JsonSerializablePayload
{
    protected $customerName;
    protected $dateOfBirth;
    protected $mobileNumber;
    protected $emailAddress;
    protected $walletStatus;
    protected $IDNumber;

    public function getCustomerName()
    {
        return $this->customerName;
    }

    public function setCustomerName($customerName)
    {
        $this->customerName = (string)$customerName;
    }

    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = (string)$dateOfBirth;
    }

    public function getMobileNumber()
    {
        return $this->mobileNumber;
    }

    public function setMobileNumber($mobileNumber)
    {
        $this->mobileNumber = (string)$mobileNumber;
    }

    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = (string)$emailAddress;
    }

    public function getWalletStatus()
    {
        return $this->walletStatus;
    }

    public function setWalletStatus($walletStatus)
    {
        $this->walletStatus = (string)$walletStatus;
    }

    public function getIDNumber()
    {
        return $this->IDNumber;
    }

    public function setIDNumber($IDNumber)
    {
        $this->IDNumber = (string)$IDNumber;
    }
}