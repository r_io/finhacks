<?php
namespace Bca\Api\Sdk\SubAccount\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class TransactionDetail extends JsonUnserializableResponse
{
    protected $transactionID;
    protected $accountStatementID;
    protected $transactionDate;
    protected $transactionType;
    protected $amount;
    protected $currencyCode;
    protected $description;
    protected $currentBalance;

    public function getTransactionID()
    {
        return $this->transactionID;
    }

    public function getAccountStatementID()
    {
        return $this->accountStatementID;
    }

    public function getTransactionDate()
    {
        return $this->transactionDate;
    }

    public function getTransactionType()
    {
        return $this->transactionType;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getCurrentBalance()
    {
        return $this->currentBalance;
    }
}