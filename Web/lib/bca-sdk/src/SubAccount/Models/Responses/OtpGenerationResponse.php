<?php
namespace Bca\Api\Sdk\SubAccount\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class OtpGenerationResponse extends JsonUnserializableResponse
{
    protected $companyCode;
    protected $customerNumber;
    protected $OTPCode;
    protected $expiredDate;
    protected $amount;

    public function getCompanyCode()
    {
        return $this->companyCode;
    }

    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }

    public function getOTPCode()
    {
        return $this->OTPCode;
    }

    public function getExpiredDate()
    {
        return $this->expiredDate;
    }

    public function getAmount()
    {
        return $this->amount;
    }
}