<?php
namespace Bca\Api\Sdk\SubAccount\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class UserRegistrationResponse extends JsonUnserializableResponse
{
    protected $primaryID;
    protected $companyCode;

    public function getPrimaryID()
    {
        return $this->primaryID;
    }

    public function getCompanyCode()
    {
        return $this->companyCode;
    }
}