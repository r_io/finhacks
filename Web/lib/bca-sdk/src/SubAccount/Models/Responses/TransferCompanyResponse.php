<?php
namespace Bca\Api\Sdk\SubAccount\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class TransferCompanyResponse extends JsonUnserializableResponse
{
    protected $companyCode;
    protected $BCAReferenceID;
    protected $transactionID;
    protected $transactionDate;

    public function getCompanyCode()
    {
        return $this->companyCode;
    }

    public function getBCAReferenceID()
    {
        return $this->BCAReferenceID;
    }

    public function getTransactionID()
    {
        return $this->transactionID;
    }

    public function getTransactionDate()
    {
        return $this->transactionDate;
    }
}