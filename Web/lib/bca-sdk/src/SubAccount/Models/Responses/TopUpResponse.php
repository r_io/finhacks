<?php
namespace Bca\Api\Sdk\SubAccount\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class TopUpResponse extends JsonUnserializableResponse
{
    protected $companyCode;
    protected $transactionID;
    protected $BCAReferenceID;
    protected $transactionDate;

    public function getCompanyCode()
    {
        return $this->companyCode;
    }

    public function getTransactionID()
    {
        return $this->transactionID;
    }

    public function getBCAReferenceID()
    {
        return $this->BCAReferenceID;
    }

    public function getTransactionDate()
    {
        return $this->transactionDate;
    }
}