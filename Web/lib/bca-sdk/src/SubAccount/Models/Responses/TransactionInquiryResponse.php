<?php
namespace Bca\Api\Sdk\SubAccount\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class TransactionInquiryResponse extends JsonUnserializableResponse
{
    public static function fromJson($json)
    {
        $result = parent::fromJson($json);
        $details = [];
        foreach ($result->transactionDetails as $jsonDetail) {
            $details[] = TransactionDetail::fromJson(json_encode($jsonDetail));
        }
        $result->transactionDetails = $details;
        return $result;
    }

    protected $companyCode;
    protected $primaryID;
    protected $totalTransactions;
    protected $lastAccountStatementID;
    protected $transactionDetails;

    public function getCompanyCode()
    {
        return $this->companyCode;
    }

    public function getPrimaryID()
    {
        return $this->primaryID;
    }

    public function getTotalTransactions()
    {
        return $this->totalTransactions;
    }

    public function getLastAccountStatementID()
    {
        return $this->lastAccountStatementID;
    }

    public function getTransactionDetails()
    {
        return $this->transactionDetails;
    }
}