<?php
namespace Bca\Api\Sdk\SubAccount\Models\Responses;

use Bca\Api\Sdk\Common\Utils\JsonUnserializableResponse;

class UserInquiryResponse extends JsonUnserializableResponse
{
    protected $primaryID;
    protected $customerNumber;
    protected $currencyCode;
    protected $balance;
    protected $customerName;
    protected $dateOfBirth;
    protected $mobileNumber;
    protected $emailAddress;
    protected $IDNumber;

    public function getPrimaryID()
    {
        return $this->primaryID;
    }

    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }

    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    public function getBalance()
    {
        return $this->balance;
    }

    public function getCustomerName()
    {
        return $this->customerName;
    }

    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    public function getMobileNumber()
    {
        return $this->mobileNumber;
    }

    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    public function getIDNumber()
    {
        return $this->IDNumber;
    }
}