<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userId')
                ->unsigned()
                ->foreign()
                ->references('id')
                ->on('users');
            $table->integer('productId')
                ->unsigned()
                ->nullable()
                ->foreign()
                ->references('id')
                ->on('products');
            $table->integer('quantity');
            $table->string('refNumber');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
